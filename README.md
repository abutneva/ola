# Pricing-Advertisement Project (OLA)

by Aleksandra Butneva, Edwin Holstein and Lan Chen

## Dependencies:

- Python 3.8
- NumPy
- sklearn
- matplotlib

## Steps

All steps are implemented within `Final Project/.` therefore one needs to cd there first.

### Step 1

```shell script
python pricing.py
```

### Step 2

```shell script
python advertisement.py
```

### Step 3

```shell script
python joint.py
```

### Step 4

- Known Structure Case

```shell script
python context_1.py
```

- Unknown Structure Case

```shell script
python context_2.py
```

### Step 5

- Sensitivity Analysis

```shell script
python sensitivity_analysis.py
```

- Comparison of USB-SW, UCB-CD & UCB-1

```shell script
python pricing_ns.py
```

### Step 6

- Comparison of USB-SW, UCB-CD & UCB-1 & EXP3

```shell script
python pricing_ns_2.py
```
