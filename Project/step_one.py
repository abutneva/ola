from user import user
from environment import Environment
from ucb1_new import UCB1_Learner
from clairvoyant import PricingOptimizer
import matplotlib.pyplot as plt
import numpy as np
#Hyperparameter
TIME_HORIZON = 365
BIDS = 100
NUMBER_OF_EXPERIMENTS = 100
NUMBER_OF_ARMS = 5
MARGIN = 0.1
PRICES = [10, 15, 20, 25, 30]
#Initialize an instance of the PricingOptimizer <=> provides clairvoyant solution

#Initialize the environment
env = Environment()
#Initialize the UCB1 learner
UCB1_learner = UCB1_Learner(PRICES)
#Initialize the users, here only C1
C1_User = user("C1")
ucb_rewards_per_experiment = []
clairvoyant_rewards_per_experiment = []
for j in range(1, NUMBER_OF_EXPERIMENTS):

    for i in range(1, TIME_HORIZON):

        pulled_arm = UCB1_learner.pull_arm() #Tuple containing the index of the pulled arm and the corresponding price
        pulled_arm_index = pulled_arm[1]
        pulled_arm_price = pulled_arm[0]
        reward = env.round(C1_User, MARGIN, pulled_arm_price, PRICES, step=1)
        clairvoyant_reward = PricingOptimizer.find_maximum_achievalbe_reward(C1_User, PRICES, MARGIN)
        UCB1_learner.update(pulled_arm_index, reward)

    ucb_rewards_per_experiment.append(UCB1_learner.collected_rewards)
    clairvoyant_rewards_per_experiment.append(clairvoyant_reward)

#Plot the reward
plt.figure(0)
plt.ylabel("Reward")
plt.xlabel("t")
plt.plot(np.mean(np.array(ucb_rewards_per_experiment), axis = 0), 'r')
plt.plot(np.mean(np.array(clairvoyant_rewards_per_experiment), axis = 0), 'b')
plt.legend(["UCB1", "Clairvoyant"])
plt.show()
#Plot the results

'''
plt.figure(0)
plt.ylabel("Regret")
plt.xlabel("t")
plt.plot(np.cumsum(np.mean(clairvoyant_reward - np.array(ucb_rewards_per_experiment), axis = 0)), 'r')
'''

