from learner import Learner
import numpy as np
class UCB1_Learner(Learner):
    def __init__(self, Arms: list):
        self.Arms = Arms
        self.len_arms = len(Arms)
        self.t = 0
        self.rewards_per_arm = [[] for i in range(self.len_arms)]
        # Initialize an empty array to store all the rewards received
        self.collected_rewards = np.array([])
    
    def pull_arm(self):
        # If we haven't pulled each arm at least once, pull the next arm
        if self.t < self.len_arms:
            return (self.Arms[self.t], self.t)
        
        # Calculate the mean reward and confidence interval for each arm
        means = [np.mean(self.rewards_per_arm[i]) for i in range(self.len_arms)]
        confidence_bounds = [np.sqrt(2*np.log(self.t) / len(self.rewards_per_arm[i])) for i in range(self.len_arms)]
        upper_confidence_bounds = [means[i] + confidence_bounds[i] for i in range(self.len_arms)]
        #Return the arm with the highest upper confidence bound using the index of the upper_cofidence_bounds list
        arm_with_UCB = self.Arms[upper_confidence_bounds.index(np.argmax(upper_confidence_bounds))]
        index_of_arm_with_UCB = upper_confidence_bounds.index([np.argmax(upper_confidence_bounds)])
        return (arm_with_UCB, index_of_arm_with_UCB)
    
    def update(self, index_of_pulled_arm, reward):
        # Update the observations and rewards for the selected arm
        self.update_observations(index_of_pulled_arm, reward)
        self.rewards_per_arm[index_of_pulled_arm].append(reward)
        self.collected_rewards = np.append(self.collected_rewards, reward)
        # Increment the time step
        self.t += 1
        
       