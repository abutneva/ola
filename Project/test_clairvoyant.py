'''
File mainly used to test the functionality of the clairvoyant class
'''
from clairvoyant import PricingOptimizer
from user import user
import numpy as np

# CReate a array of 100 values evenly spread between 0 and 10 and dont use numpy
Bids = []
for i in range(0, 100):
    Bids.append(i/20)
print(Bids)
Pricingoptimizer = PricingOptimizer(num_classes=10, bids=Bids, contexts=10)
C1_User = user("C1")
Prices = [10, 15, 20, 25, 30]
# Create a array of 100 values evenly spread between 0 and 10 and use numpy

Price_Rewards = Pricingoptimizer.find_rewards(C1_User, Prices, 1)
for index, val in enumerate(Price_Rewards):
    print(f"Index {index} => Price {val[0]} Bid {val[1]} Reward {val[2]}")
'''
Testing the find best price function of the pricing optimizer class
'''
#Print the index of the value inside Price rewards with the highest reward which is the 3rd value of the array inside the proce rewards array
PricesNP = np.array(Price_Rewards)
print(f"Best price at index: {np.argmax(PricesNP[:, 2])}")
#create a variable that saces the third value of the array inside the array
Best_Price = PricesNP[np.argmax(PricesNP[:, 2])][0]
print(f"Output of the best price function that encapsulates the reward function {Pricingoptimizer.find_best_price(user=C1_User, prices=Prices, margin=1)}")
print(f"best price of: {Best_Price}")
corresponding_bid = PricesNP[np.argmax(PricesNP[:, 2])][1]
print(f"corresponding bid of: {corresponding_bid}")
corresponding_reward = PricesNP[np.argmax(PricesNP[:, 2])][2]
print(f"corresponding reward of: {corresponding_reward}")
'''
Testing the find best bid function of the pricing optimizer class (may not be needed?!)
'''
#Create a array of 100 values evenly spread between 0 and 10 and dont use numpy
Best_Bid = Pricingoptimizer.find_rewards_for_bids(C1_User, Best_Price, 1)
for index, val in enumerate(Best_Bid):
    print(f"Index {index} => Bid {val[0]} Reward {val[1]}")
#Print the index of the value inside Price rewards with the highest reward which is the 2rd value of the array inside the proce rewards array
Best_Bid = np.array(Best_Bid)
print(f"Best bid at index: {np.argmax(Best_Bid[:, 1])}")
#And print the b est bid 
print(f"Best bid of: {Best_Bid[np.argmax(Best_Bid[:, 1])][0]}")

