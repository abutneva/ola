import numpy as np

class Learner():
    def __init__(self, Arms: list):
        self.Arms = Arms
        self.len_arms = len(Arms)
        self.t = 0
        self.rewards_per_arm = [[] for i in range(self.len_arms)]
        # Initialize an empty array to store all the rewards received
        self.collected_rewards = np.array([])
    def update_observations(self, index_of_pulled_arm, reward):
        self.t += 1
        self.rewards_per_arm[index_of_pulled_arm].append(reward) #recall: python list 
        self.collected_rewards = np.append(self.collected_rewards, reward) #recall: numpy array

    
    