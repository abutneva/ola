from user import user
import numpy as np
class price():
    def __init__(self, user_class: user):
        '''
        
        '''
        self.prices: dict = {
            # The keys are placeholder for the actual prices
            # The values resemble boolean values that indicate whether a user buys the product 
            15: bool(np.random.binomial(n=1, p=user.get_conversion_rate(user.conversion_rate_model, 15))),
            20: bool(np.random.binomial(n=1, p=user.get_conversion_rate(user.conversion_rate_model, 20))), 
            25: bool(np.random.binomial(n=1, p=user.get_conversion_rate(user.conversion_rate_model, 25))), 
            30: bool(np.random.binomial(n=1, p=user.get_conversion_rate(user.conversion_rate_model, 30))), 
            35: bool(np.random.binomial(n=1, p=user.get_conversion_rate(user.conversion_rate_model, 35)))
            }
       
        
