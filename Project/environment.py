from user import user
from clairvoyant import PricingOptimizer
class Environment():
    
    def __init__(self):
        #Constructor
        pass

    
    
    def round(self, user, margin, price, prices, step: int):
        # Arm resembles a user and price because they are both needed to calculate a reward
        if step == 1:
            reward = PricingOptimizer.find_max_reward_and_bid_given_a_price(user=user, prices=prices, picked_price=price, margin=margin)
        else:
            reward = PricingOptimizer.reward(clicks=user.get_clicks(), conversion_prob=user.get_conversion_rate(price), margin=margin, cost=user.get_cost(price))
        return reward
    