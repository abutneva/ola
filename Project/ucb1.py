'''
This implementation is a UCB1 learner that selects the arm with the highest expected reward. The algorithm balances exploration and exploitation by selecting the arm with the highest upper confidence bound. The upper confidence bound is calculated as the mean reward plus a confidence interval that decreases as more observations are made. The pull_arm method selects the arm with the highest upper confidence bound, while the update method updates the observations and rewards for the selected arm.
'''


from learner import Learner
import numpy as np

class UCB1_Learner(Learner):
    def __init__(self, n_arms):
        super().__init__(n_arms)
        self.t = 0
        # Initialize an empty list for each arm to store the rewards received
        self.rewards_per_arm = [[] for i in range(n_arms)]
        # Initialize an empty array to store all the rewards received
        self.collected_rewards = np.array([])
    
    def pull_arm(self):
        # If we haven't pulled each arm at least once, pull the next arm
        if self.t < self.n_arms:
            return self.t
        
        # Calculate the mean reward and confidence interval for each arm
        means = [np.mean(self.rewards_per_arm[i]) for i in range(self.n_arms)]
        confidence_bounds = [np.sqrt(2*np.log(self.t) / len(self.rewards_per_arm[i])) for i in range(self.n_arms)]
        upper_confidence_bounds = [means[i] + confidence_bounds[i] for i in range(self.n_arms)]
        
        # Pull the arm with the highest upper confidence bound
        return np.argmax(upper_confidence_bounds)
    
    def update(self, index_of_pulled_arm, reward):
        # Update the observations and rewards for the selected arm
        self.update_observations(index_of_pulled_arm, reward)
        self.rewards_per_arm[index_of_pulled_arm].append(reward)
        self.collected_rewards = np.append(self.collected_rewards, reward)
        # Increment the time step
        self.t += 1

