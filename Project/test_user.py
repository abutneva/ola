from user import user
from prices import price

#Hyperparameters
TIME_HORIZON = 365
#Drei Klassen: C1, C2, C3 initialisieren
C1_User = user("C1")
C2_User = user("C2")
C3_User = user("C3")
print(C1_User.conversion_rate_model)
""" for i in range(1, 35):
    print(C1_User.get_conversion_rate(C1_User.conversion_rate_model, i)) """
'''
C1_Prices_And_conversion = price(C1_User).prices()
print(C1_Prices_And_conversion)
'''

#Plotting every conversion rate for prices of 1 to 35 of every user using matplotlib

import matplotlib.pyplot as plt
import numpy as np
C1_Prices = []
C2_Prices = []
C3_Prices = []
for i in range(1, 35):
    C1_Prices.append(C1_User.get_conversion_rate(i))
    C2_Prices.append(C2_User.get_conversion_rate(i))
    C3_Prices.append(C3_User.get_conversion_rate(i))
X = np.arange(1, 35)
plt.plot(X, C1_Prices, label = "C1")
plt.plot(X, C2_Prices, label = "C2")
plt.plot(X, C3_Prices, label = "C3")
plt.xlabel("Price")
plt.ylabel("Conversion Rate")
plt.title("Conversion Rate for different prices and users")
plt.legend()
plt.show()

#Plot the historical conversion rates for every user

import matplotlib.pyplot as plt
import numpy as np
plt.plot(list(C1_User.historical_conversion.keys()), list(C1_User.historical_conversion.values()), label = "C1")
plt.plot(list(C2_User.historical_conversion.keys()), list(C2_User.historical_conversion.values()), label = "C2")
plt.plot(list(C3_User.historical_conversion.keys()), list(C3_User.historical_conversion.values()), label = "C3")
plt.xlabel("Price")
plt.ylabel("Conversion Rate")
plt.title("Historical Conversion Rate for different prices and users")
plt.legend()
plt.show()

# Plot the click funktion for bids of 1 to 35 for every user
C1_Clicks = []
C2_Clicks = []
C3_Clicks = []
for i in range(1, 35):
    C1_Clicks.append(C1_User.get_clicks(i))
    C2_Clicks.append(C2_User.get_clicks(i))
    C3_Clicks.append(C3_User.get_clicks(i))
X = np.arange(1, 35)
plt.plot(X, C1_Clicks, label = "C1")
plt.plot(X, C2_Clicks, label = "C2")
plt.plot(X, C3_Clicks, label = "C3")
plt.xlabel("Bid")
plt.ylabel("Clicks")
plt.title("Clicks for different bids and users")
plt.legend()
plt.show()

#Plot the cost function for bids of 1 to 35 for every user
C1_Cost = []
C2_Cost = []
C3_Cost = []
for i in range(1, 35):
    C1_Cost.append(C1_User.get_cost(i))
    C2_Cost.append(C2_User.get_cost(i))
    C3_Cost.append(C3_User.get_cost(i))
X = np.arange(1, 35)
plt.plot(X, C1_Cost, label = "C1")
plt.plot(X, C2_Cost, label = "C2")
plt.plot(X, C3_Cost, label = "C3")
plt.xlabel("Bid")
plt.ylabel("Cost")
plt.title("Cost for different bids and users")
plt.legend()
plt.show()

