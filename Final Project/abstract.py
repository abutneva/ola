
import numpy as np
from abc import ABC, abstractmethod
from util import colours
from typing import List, Dict
from sklearn.gaussian_process import GaussianProcessRegressor
from sklearn.gaussian_process.kernels import RBF, ConstantKernel as C
Colours = colours()

class abc_process(ABC):
    '''
    This class resembles an abstract sceleton/parent for both the advertisment and pricing bandit implementations 
    Furthermore it implements the configuration and defines our setup, meaning the conversion rates per user class, the cost per user class and the price per user class
    Other than that it also defines the feature set, that the contexts are derived from
    '''

    '''
    Abstract class for the process
    Abstract methods that do not have an implementation in the abstract class:
    - get_name
    - pull_arm
    - get_target_value
    - update_parameters
    '''
    
    def __init__(self, n_arms: int, user_class: str):
        self.user_class = user_class
        self.__name__ = self.get_name()
        self.n_arms = n_arms
        self.t = 0
        self.configuration = self.read_configuration(user_class)
        self.bids = self.get_bids()
        print("----------------")
        print(f"{self.__name__} process initialization")  
    
    # defines the class name, e.g., clairvoyant, TS
    @abstractmethod
    def get_name(self)->str:
        pass
   
    
    @abstractmethod
    def read_configuration(self, user_class: str)->dict:  
        contexts = self.get_contexts()
        bids = self.get_bids()
        prices = self.get_prices()
        cost_curve = self.get_cost_curve(user_class)
        price_curve_for_user = { #Needed for joint context learning where conversion differ depending on the user class
            'C1': self.get_price_curve('C1'),
            'C2': self.get_price_curve('C2'),
            'C3': self.get_price_curve('C3')
        }
        cost_curve_for_user = { #Needed for joint context learning where conversion differ depending on the user class
            'C1': self.get_cost_curve('C1'),
            'C2': self.get_cost_curve('C2'),
            'C3': self.get_cost_curve('C3')
        }
        click_curve_for_user = {
            'C1': self.get_click_curve('C1'),
            'C2': self.get_click_curve('C2'),
            'C3': self.get_click_curve('C3')

        }
        click_curve = self.get_click_curve(user_class)
        price_curve = self.get_price_curve(user_class)
        configuration = {
            'contexts': contexts,
            'bids': bids,
            'prices': prices,
            'cost_curve': cost_curve,
            'price_curve_for_user': price_curve_for_user,
            'cost_curve_for_user': cost_curve_for_user,
            'click_curve_for_user': click_curve_for_user,
            'click_curve': click_curve,
            'price_curve': price_curve
        }
        return configuration
    def change_configuration(self, season):
        '''
        Changes the configuration of the process depending on the season
        '''
        if season == "beachbody":
            self.configuration["click_curve"] = self.get_click_curve(self.user_class, season)
            self.configuration["price_curve"] = self.get_price_curve(self.user_class, season)
            self.configuration["click_curve_for_user"] = {
                'C1': self.get_click_curve('C1', season='beachbody'),
                'C2': self.get_click_curve('C2', season='beachbody'),
                'C3': self.get_click_curve('C3', season='beachbody')
            }
            self.configuration["price_curve_for_user"] = {
                'C1': self.get_price_curve('C1', season='beachbody'),
                'C2': self.get_price_curve('C2', season='beachbody'),
                'C3': self.get_price_curve('C3', season='beachbody')
            }
        elif season == "new year resolution":
            self.configuration["click_curve"] = self.get_click_curve(self.user_class, season)
            self.configuration["price_curve"] = self.get_price_curve(self.user_class, season)
            self.configuration["click_curve_for_user"] = {
                'C1': self.get_click_curve('C1', season='new year resolution'),
                'C2': self.get_click_curve('C2', season='new year resolution'),
                'C3': self.get_click_curve('C3', season='new year resolution')
            }
            self.configuration["price_curve_for_user"] = {
                'C1': self.get_price_curve('C1', season='new year resolution'),
                'C2': self.get_price_curve('C2', season='new year resolution'),
                'C3': self.get_price_curve('C3', season='new year resolution')
            }
        elif season == "lazy":
            self.configuration["click_curve"] = self.get_click_curve(self.user_class, season=None)
            self.configuration["price_curve"] = self.get_price_curve(self.user_class, season=None)
            self.configuration["click_curve_for_user"] = {
                'C1': self.get_click_curve('C1', season=None),
                'C2': self.get_click_curve('C2', season=None),
                'C3': self.get_click_curve('C3', season=None)
            }
            self.configuration["price_curve_for_user"] = {
                'C1': self.get_price_curve('C1', season=None),
                'C2': self.get_price_curve('C2', season=None),
                'C3': self.get_price_curve('C3', season=None)
            }
            

    def return_arm_for_price(self, price: float)->int:
        '''
        Maps the price to an arm
        '''
        return self.configuration['prices'].index(price)
    
    def return_price_for_arm(self, arm: int)->float:
        '''
        Maps the arm to a price
        '''
        return self.configuration['prices'][arm]
    
    def get_contexts(self):
        '''
        This method is not being used but provides the feature set for the contexts
        Note: C3 has two feature sets
        F1: Professional athlete or not
        F2: Gender male 0 or female 1
        '''
        # return contexts
        contexts = []
        # C1
        contexts.append({

            'F1': 0, # professional athlete or no
            'F2': 0  # gender
        })
        # C2
        contexts.append({
            # female amateurs
            'F1': 0,
            'F2': 1
        })
        # C3
        contexts.append({
            # male & female professionals
            'F1': 1,
            'F2': 1
           # F1: 1
           # F2: 1

            

        })
        return contexts
    
    def get_bids(self):
        # return bids
        min_bid = 0.01
        max_bid = 2
        bids = np.linspace(min_bid, max_bid, num=100)
        return bids
    
    def get_prices(self):
        '''
        Returns the prices
        Note that the prices are the same for all user classes or scenarios
        '''
        return [34, 37, 40, 44, 47]
    
    def get_cost_curve(self, user_class: str)->dict:
        '''
        return the cost curve depending on the user class as a dictionary
        As a dictionary with the bid as key and the cost as value
        '''
        cost_curve = {}
        for bid in self.get_bids():
            if user_class == "C1": 
                cost = 150*np.log(10*(bid**0.5)+1)
                #cost = 175*np.log(5.2*bid + 1)
            elif user_class == "C2":
                cost = 150*np.log(8*(bid**0.5)+1)
                #cost = 175*np.log(5.15*bid + 1)
            elif user_class == "C3":
                cost = 300*np.log(8*(bid**0.5)+1)
                #cost = 350*np.log(5*bid + 1)
            # Adding Gaussian noise to the average curve
            cost += np.random.normal(0, 1)
            cost_curve[bid] = cost
        return cost_curve
    
        
    def get_click_curve(self, user_class: str, season:  str = None)->dict:
        '''
        return the click curve depending on the user class as a dictionary
        As a dictionary with the bid as key and the cost as value
        '''
        if season == "beachbody":
            click_curve = {}
            for bid in self.get_bids():
                # C1 
                if user_class == "C1":
                    #click are 700*ln(5.2*bid + 1)
                    clicks = 675-75*((bid-3)**2)
                    #clicks = 700*np.log(5.2*bid + 1)
                # C2
                elif user_class == "C2":
                    """ if bid < 1.17 and bid > 0.76:
                        clicks = 4750*(bid**2)+9500*bid-4400
                    else:
                        clicks = 50*(bid**3)+92*(bid**2) """
                    clicks = 450-40*((bid-3)**2) 
                # C3
                elif user_class == "C3":
                    clicks = 840-40*((bid-3)**2)
                clicks += np.random.normal(0, 1)
                click_curve[bid] = clicks
        elif season == "new year resolution":
            click_curve = {}
            for bid in self.get_bids():
                # C1 
                if user_class == "C1":
                    #click are 700*ln(5.2*bid + 1)
                    clicks = 900-100*((bid**3)**2)
                    #clicks = 700*np.log(5.2*bid + 1)
                # C2
                elif user_class == "C2":
                    """ if bid < 1.17 and bid > 0.76:
                        clicks = 4750*(bid**2)+9500*bid-4400
                    else:
                    clicks = 50*(bid**3)+92*(bid**2) """
                    clicks = 450-40*((bid-3)**2) 

                # C3
                elif user_class == "C3":
                    clicks = 840-40*((bid-3)**2)

                clicks += np.random.normal(0, 1)
                click_curve[bid] = clicks

        else:
            click_curve = {}
            for bid in self.get_bids():
                # C1 
                if user_class == "C1":
                    #click are 700*ln(5.2*bid + 1)
                    clicks = 450-50*((bid-3)**2)
                    #clicks = 700*np.log(5.2*bid + 1)
                # C2
                elif user_class == "C2":
                    """ if bid < 1.17 and bid > 0.76:
                        clicks = 4750*(bid**2)+9500*bid-4400
                    else:
                        clicks = 50*(bid**3)+92*(bid**2) """
                    clicks = 450-40*((bid-3)**2) 
                # C3
                elif user_class == "C3":
                    clicks = 840-40*((bid-3)**2) 
                # Adding Gaussian noise to the average curve
                clicks += np.random.normal(0, 1)
                click_curve[bid] = clicks

        return click_curve



    
    def get_price_curve(self, user_class:str, season: str = None)->dict:
        # return price curve
        prices = self.get_prices()
        price_curve = {}
        if season == None:
            for price in prices:
                # Compute the probability of conversion for each price
                if user_class == 'C1':
                    # For C1
                    if price == prices[0]:
                        price_curve[price] = 0.12
                    elif price == prices[1]:
                        price_curve[price] = 0.08
                    elif price == prices[2]:
                        price_curve[price] = 0.07
                    elif price == prices[3]:
                        price_curve[price] = 0.065
                    elif price == prices[4]:
                        price_curve[price] = 0.06
                elif user_class == 'C2':
                    # For C2
                    if price == prices[0]:
                        price_curve[price] = 0.06
                    elif price == prices[1]:
                        price_curve[price] = 0.065
                    elif price == prices[2]:
                        price_curve[price] = 0.08
                    elif price == prices[3]:
                        price_curve[price] = 0.07
                    elif price == prices[4]:
                        price_curve[price] = 0.065
                elif user_class == 'C3':
                    # For C3
                    if price == prices[0]:
                        price_curve[price] = 0.08
                    elif price == prices[1]:
                        price_curve[price] = 0.075
                    elif price == prices[2]:
                        price_curve[price] = 0.07
                    elif price == prices[3]:
                        price_curve[price] = 0.065
                    elif price == prices[4]:
                        price_curve[price] = 0.06
        elif season == "beachbody":
             for price in prices:
                # Compute the probability of conversion for each price
                if user_class == 'C1':
                    # For C1
                    if price == prices[0]:
                        price_curve[price] = 0.12 *2
                    elif price == prices[1]:
                        price_curve[price] = 0.08 *3.5
                    elif price == prices[2]:
                        price_curve[price] = 0.07 *2
                    elif price == prices[3]:
                        price_curve[price] = 0.065 *1.5
                    elif price == prices[4]:
                        price_curve[price] = 0.06 *1.25
                elif user_class == 'C2':
                    # For C2
                    if price == prices[0]:
                        price_curve[price] = 0.06
                    elif price == prices[1]:
                        price_curve[price] = 0.065
                    elif price == prices[2]:
                        price_curve[price] = 0.08
                    elif price == prices[3]:
                        price_curve[price] = 0.07
                    elif price == prices[4]:
                        price_curve[price] = 0.065
                elif user_class == 'C3':
                    # For C3
                    if price == prices[0]:
                        price_curve[price] = 0.08
                    elif price == prices[1]:
                        price_curve[price] = 0.075
                    elif price == prices[2]:
                        price_curve[price] = 0.07
                    elif price == prices[3]:
                        price_curve[price] = 0.065
                    elif price == prices[4]:
                        price_curve[price] = 0.06
        elif season == "new year resolution":
            for price in prices:
                # Compute the probability of conversion for each price
                if user_class == 'C1':
                    # For C1
                    if price == prices[0]:
                        price_curve[price] = 0.12 
                    elif price == prices[1]:
                        price_curve[price] = 0.08 * 2
                    elif price == prices[2]:
                        price_curve[price] = 0.07 * 3
                    elif price == prices[3]:
                        price_curve[price] = 0.065 * 4
                    elif price == prices[4]:
                        price_curve[price] = 0.06 * 4.5
                elif user_class == 'C2':
                    # For C2
                    if price == prices[0]:
                        price_curve[price] = 0.06
                    elif price == prices[1]:
                        price_curve[price] = 0.065
                    elif price == prices[2]:
                        price_curve[price] = 0.08
                    elif price == prices[3]:
                        price_curve[price] = 0.07
                    elif price == prices[4]:
                        price_curve[price] = 0.065
                elif user_class == 'C3':
                    # For C3
                    if price == prices[0]:
                        price_curve[price] = 0.08
                    elif price == prices[1]:
                        price_curve[price] = 0.075
                    elif price == prices[2]:
                        price_curve[price] = 0.07
                    elif price == prices[3]:
                        price_curve[price] = 0.065
                    elif price == prices[4]:
                        price_curve[price] = 0.06
        elif season == "4th season":
            for price in prices:
                if user_class == 'C1':
                    # For C1
                    if price == prices[0]:
                        price_curve[price] = 0.12 
                    elif price == prices[1]:
                        price_curve[price] = 0.08 * 1.5
                    elif price == prices[2]:
                        price_curve[price] = 0.07 * 2
                    elif price == prices[3]:
                        price_curve[price] = 0.065 * 1.5
                    elif price == prices[4]:
                        price_curve[price] = 0.06 
                
                elif user_class == 'C2':
                    # For C2
                    if price == prices[0]:
                        price_curve[price] = 0.06
                    elif price == prices[1]:
                        price_curve[price] = 0.065
                    elif price == prices[2]:
                        price_curve[price] = 0.08
                    elif price == prices[3]:
                        price_curve[price] = 0.07
                    elif price == prices[4]:
                        price_curve[price] = 0.065
                elif user_class == 'C3':
                    # For C3
                    if price == prices[0]:
                        price_curve[price] = 0.08
                    elif price == prices[1]:
                        price_curve[price] = 0.075
                    elif price == prices[2]:
                        price_curve[price] = 0.07
                    elif price == prices[3]:
                        price_curve[price] = 0.065
                    elif price == prices[4]:
                        price_curve[price] = 0.06
        elif season == "5th season":
            for price in prices:
                if user_class == 'C1':
                    # For C1
                    if price == prices[0]:
                        price_curve[price] = 0.12 * 1.25
                    elif price == prices[1]:
                        price_curve[price] = 0.08 * 2
                    elif price == prices[2]:
                        price_curve[price] = 0.07 * 2.5
                    elif price == prices[3]:
                        price_curve[price] = 0.065 * 3
                    elif price == prices[4]:
                        price_curve[price] = 0.06 * 2
                elif user_class == 'C2':
                    # For C2
                    if price == prices[0]:
                        price_curve[price] = 0.06
                    elif price == prices[1]:
                        price_curve[price] = 0.065
                    elif price == prices[2]:
                        price_curve[price] = 0.08
                    elif price == prices[3]:
                        price_curve[price] = 0.07
                    elif price == prices[4]:
                        price_curve[price] = 0.065
                elif user_class == 'C3':
                    # For C3
                    if price == prices[0]:
                        price_curve[price] = 0.08
                    elif price == prices[1]:
                        price_curve[price] = 0.075
                    elif price == prices[2]:
                        price_curve[price] = 0.07
                    elif price == prices[3]:
                        price_curve[price] = 0.065
                    elif price == prices[4]:
                        price_curve[price] = 0.06
        return price_curve
    
    def get_reward(self, user_class: str, price: int, bid: int)->int:
        '''
        Most simple form of the reward function
        Assumes both bid and price to be known
        Note that either those values may be unkown, which requires a breadth-first search (Clairvoyant)
        '''
        reward = 0
        click_curve = self.configuration['click_curve_for_user'][user_class]
        cost_curve = self.configuration['cost_curve_for_user'][user_class]
        price_curve = self.configuration['price_curve_for_user'][user_class]
        num_clicks = click_curve[bid]
        cost = cost_curve[bid]
        conv_prob = price_curve[price]
        margin = 20 # constant value
        reward += num_clicks * conv_prob * margin - cost
        return reward
    
    def get_phases(self):
        # return phases
        # consider that the product in question is protein powder when reasoning about seasonality
        phases = []
        # Phase 1: New Year's resolutions
        # This phase corresponds to the period between January and March, when people are looking to get in shape and make healthier lifestyle choices. 
        phase1 = {
            'name': 'New Year\'s resolutions',
            'period': 'January - March',
            'optimal_price': 'Very high'
        }
        phases.append(phase1)
        # Phase 2: Summer
        # This phase corresponds to the period between April and August, when people are looking to maintain shape for the summer. 
        phase2 = {
            'name': 'Summer',
            'period': 'April - August',
            'optimal_price': 'Medium'
        }
        phases.append(phase2)
        # Phase 3: Pre-holiday season
        # This phase corresponds to the period between September and October, when people are looking to get in shape for the holidays. 
        phase3 = {
            'name': 'Pre-holiday season',
            'period': 'September - October',
            'optimal_price': 'Very low'
        }
        phases.append(phase3)
        # Phase 4: Holiday season
        # This phase corresponds to the period between November and December, when people are looking to get in shape for the holidays. 
        phase4 = {
            'name': 'Holiday season',
            'period': 'November - December',
            'optimal_price': 'High'
        }
        phases.append(phase4)
        # Phase 5: Post-holiday season
        # This phase corresponds to the period between January and February, when people are looking to get back in shape after the holidays. 
        phase5 = {
            'name': 'Post-holiday season',
            'period': 'January - February',
            'optimal_price': 'Low'
        }
        phases.append(phase5)
        return phases
    
    @abstractmethod
    def get_target_value(self, configuration, reward):
        pass

    
    @abstractmethod
    def pull_arm(self, parameters: dict, objective_function, used_algorithm: str)->int:
        pass

    @abstractmethod
    def update_parameters(self, reward: list, pull_arm: int)->dict:
        pass

'''
The following class is used for the Pricing Bandits
'''

class mock_process(abc_process):
    def __init__(self, n_arms : int, used_algorithm: str, user_class: str, window_size = None, gamma = None):
        super().__init__(n_arms, user_class)
        self.empirical_means = np.zeros(self.n_arms)
        #Confidence is needed for UCB
        self.confidence = np.array([np.inf]*self.n_arms)
        #Prior(Beta) is needed for TS
        self.beta_parameters = np.ones((n_arms, 2)) # size of numpy array is 2 to store both beta parameters
        self.used_algorithm:str = used_algorithm
        self.rewards_per_arm = x = [[] for arm in range(self.n_arms)]
        #Sliding window size is needed for UCB
        self.window_size = window_size
        #History of arms that were pulled is needed for SW-UCB
        self.pulled_arms= np.array([]) # initialize as empty array
        #EXP3 parameters
        self.gamma = gamma
        self.weights = np.ones(self.n_arms)
        self.probabilities = np.ones(self.n_arms) / self.n_arms


    def get_name(self) -> str:
        #Name defines which curve is learned
        return "Pricing"
    
    def get_n_arms(self) -> int:
        return super().get_n_arms()
    
    def read_configuration(self, user_class: str) -> dict:
        return super().read_configuration(user_class)
    
    def pull_arm(self) -> int:
        '''
        Funtion returns an arm depending on the used algorithm
        IF UCB: pull arm with Upper Confidence Bound
        Returns the arm as a value in between 0 and n_arms
        '''
        if self.used_algorithm == "UCB":
            #pull arm with Upper Confidence Bound
            #calculate upper confidence bound as empirical means plus confidence
            upper_conf = self.empirical_means + self.confidence
            #decide upon the winning arm
            pulled_arm = np.random.choice(np.where(upper_conf == upper_conf.max())[0])
            #save chosen arm in the list of pulled arms for later use
            self.pulled_arms = np.append(self.pulled_arms, pulled_arm)
            #return the arm with highest UCB
            return pulled_arm
        
        elif self.used_algorithm == "SW-UCB":
            #pull arm with Upper Confidence Bound
            #calculate upper confidence bound as empirical means plus confidence
            upper_conf = self.empirical_means + self.confidence
            #decide upon the winning arm
            pulled_arm = np.random.choice(np.where(upper_conf == upper_conf.max())[0])
            #save chosen arm in the list of pulled arms for later use
            self.pulled_arms = np.append(self.pulled_arms, pulled_arm)
            #return the arm with highest UCB
            return pulled_arm

        elif self.used_algorithm == "CD-UCB":
            #pull arm with Upper Confidence Bound
            #calculate upper confidence bound as empirical means plus confidence
            upper_conf = self.empirical_means + self.confidence
            #decide upon the winning arm
            pulled_arm = np.random.choice(np.where(upper_conf == upper_conf.max())[0])
            #save chosen arm in the list of pulled arms for later use
            self.pulled_arms = np.append(self.pulled_arms, pulled_arm)
            #return the arm with highest UCB
            return pulled_arm
        elif self.used_algorithm == "EXP3":
            #pull arm with highest probability
            pulled_arm = np.random.choice(np.arange(self.n_arms), p=self.probabilities)
            return pulled_arm
        
        elif self.used_algorithm == "TS":
            # select the index of the max value
            # get beta parameters for selected arm 
            idx = np.argmax(np.random.beta(self.beta_parameters[:,0], self.beta_parameters[:,1]))
            return idx
        


        
    def get_target_value(self, price: int, user_class = None)-> float:
        '''
        Reward is either 0 or 1 depending on whether the user converts or not (above/below 0.5 in his conversion probability)
        '''
        if user_class != None:
            user_specific_price_curve = self.configuration["price_curve_for_user"][user_class]
            conv_prob = user_specific_price_curve[price]
            return np.random.binomial(1, conv_prob)
        reward = np.random.binomial(1, self.configuration["price_curve"][price]) # success/conversion probability 
        return reward
    
    def update_parameters(self, reward: int, pull_arm: int, cum_rew=None, tau=None)->dict:
        '''
        
        Depending on the algorithm that was set during initialization:
        UCB: Receives the reward and appends the reward to the list of rewards for the arm and updates the empirical mean and the confidence
        TS: Receives the reward and updates the beta parameters for the arm
        '''
        self.cum_rew = cum_rew # needed for SW

        if self.used_algorithm == "UCB":
            self.t += 1
            self.rewards_per_arm[pull_arm].append(reward)
            self.empirical_means[pull_arm] = (self.empirical_means[pull_arm]*(self.t-1) + reward)/self.t
            self.confidence[pull_arm] = np.sqrt(2*np.log(self.t)/len(self.rewards_per_arm[pull_arm]))

        elif self.used_algorithm == "SW-UCB":
            self.t = self.t + 1
            self.rewards_per_arm[pull_arm].append(reward)
            for arm in range(self.n_arms):
                #nr of samples that are relevant for sw
                n_samples = np.sum(self.pulled_arms[-self.window_size:] == arm)
                #cumulative reward within nr of samples in 1 sw
                cum_rew = np.sum(self.rewards_per_arm[arm][-n_samples:]) if n_samples > 0 else 0
                #update empirical means and confidence estimates
                self.empirical_means[arm] = cum_rew/n_samples if n_samples > 0 else 0
                self.confidence[arm] = np.sqrt(2*np.log(self.t)/n_samples) if n_samples > 0 else 0

        elif self.used_algorithm == "CD-UCB":
            self.t = self.t + 1
            if len(self.rewards_per_arm[pull_arm]) >= tau:
                detect_change = self.detect_change(pull_arm, reward, tau)
                if detect_change:
                    self.reset_arm(pull_arm)
            self.rewards_per_arm[pull_arm].append(reward)
            self.empirical_means[pull_arm] = (self.empirical_means[pull_arm]*(self.t-1) + reward)/self.t
            self.confidence[pull_arm] = np.sqrt(2*np.log(self.t)/len(self.rewards_per_arm[pull_arm]))

        elif self.used_algorithm == "EXP3":
            self.t = self.t + 1
            #Update the probabilities of all arms
            self.weights[pull_arm] *= np.exp(self.gamma * reward / self.n_arms)
            self.probabilities = self.weights / np.sum(self.weights)
            """ print(self.probabilities)
            print(self.weights) """
            

        elif self.used_algorithm == "TS":
            # update the pars of Beta dist for each arm
            ## 1 par counts how many successes (1) we had
            ## 2 par counts how many losses (0) we had
            self.beta_parameters[pull_arm, 0] = self.beta_parameters[pull_arm, 0] + reward
            self.beta_parameters[pull_arm, 1] = self.beta_parameters[pull_arm, 1] + 1 - reward
 
            


    def detect_change(self, pull_arm, reward, tau):
        '''
        Method detects a change in the conversion by checking earlier conversions for tau steps in the past 
        If 50% of the conversions differ, we detect a change
        '''
        counter = 0
        for i in range(tau):
            if self.rewards_per_arm[pull_arm][-i] != reward:
                counter += 1
        if counter >= tau/2:
            return True
        else:
            return False
        
    def reset_arm(self, pull_arm):
        self.empirical_means[pull_arm] = np.sum(self.empirical_means)/self.n_arms
        self.confidence[pull_arm] = np.sum(self.confidence)/self.n_arms
        self.rewards_per_arm[pull_arm] = []
                

        
    def initialize(self):
        '''
        Utility class to initialize the Cofidence intervals
        We retrieve one reward per arm according to UCB1
        Note: This is only needed if UCB is used
        '''
        if self.used_algorithm == "UCB" or self.used_algorithm == "SW-UCB":
            print(f"Initializing {self.used_algorithm}...")
            for i in range(0, self.n_arms-1):
                price = self.return_price_for_arm(i)
                reward = self.get_target_value(price)
                self.rewards_per_arm[i].append(reward)

        else:
            pass




class clairvoyant_process(abc_process):
    def __init__(self, n_arms: int, user_class: str):
        super().__init__(n_arms, user_class)
        self.rewards_per_arm = x = [[] for arm in range(self.n_arms)]
        self.best_bids = dict()
    def get_name(self) -> str:
        return "Clairvoyant Pricing"
    
    def pull_best_arm(self) -> tuple:
        '''
        Function returns the arm with the highest reward and the reward itself
        Best arm is at index 0
        Best Reward is at index 1
        '''
        best_bids = dict()
        for bid in self.get_bids():
            best_bids[bid] = dict()
            for price in self.configuration["prices"]:
                reward = self.get_reward(self.user_class, price, bid)
                best_bids[bid][price]= reward
                #return the price index with the highest reward
        #Save best bids for later use (Calculating the best bid for the suboptimal price returned by UCB and TS) => To calculate a reward that is comparable
        self.best_bids = best_bids
        best_reward = self.find_best_price_and_reward(best_bids)[0]
        best_price = self.find_best_price_and_reward(best_bids)[1]
        best_arm = self.return_arm_for_price(best_price)
        return (best_arm, best_reward)
    
    def get_reward_given_a_fix_price(self, price: int) -> tuple:
        #reward initialized with -inf
        best_reward = -np.inf
        for bid in self.get_bids():
            reward = self.get_reward(self.user_class, price, bid)
            if reward > best_reward:
                best_reward = reward
                best_bid = bid
            return best_reward
    def get_price_given_a_fix_bid(self, bid: int): 
        for price in self.configuration["prices"]:
            reward = self.get_reward(self.user_class, price, bid)
            if reward > best_reward:
                best_reward = reward
                best_price = price
            return best_price, best_reward
    
    def get_optimal_candidate(self, scenario: str) -> tuple:
        '''
        Scenario <=> "joint"
        This function returns the best candidate by looking at ehich combination of price and bid returns the highest reward
        The best candidate is a tuple consisting of the price, bid and reward
        [0] = price
        [1] = bid
        [2] = reward
        Scenario <=> "pricing"
        This function returns the best candidate by looking at which price returns the highest conversion probability
        The best candidate is a integer consisting of the price
        [0] = price
        Scenario <=> "advertising"
        This function returns the best candidate by looking at: 
        - which bid returns the most amount of clicks at index 0
        - which bid returns the lowest  costs at index 1
        [0] = bid with most clicks
        [1] = bid with lowest costs
        The solution of this function can be used as the clairvoyant option in all of the scenarios (When learning pricing or advertisement or jointly)
        '''
        if scenario == "joint":
            best_bids = dict()
            for bid in self.get_bids():
                best_bids[bid] = dict()
                for price in self.get_prices():
                    reward = self.get_reward(self.user_class, price, bid)
                    best_bids[bid][price]= reward
                    #return the price index with the highest reward
            #Save best bids for later use (Calculating the best bid for the suboptimal price returned by UCB and TS) => To calculate a reward that is comparable
            self.best_bids = best_bids
            best_reward = self.find_best_price_and_reward(best_bids)[0]
            best_price = self.find_best_price_and_reward(best_bids)[1]
            best_bid = self.find_best_price_and_reward(best_bids)[2]
            best_arm = self.return_arm_for_price(best_price)
            return (best_price, best_bid, best_reward)
        elif scenario == "pricing":
            best_conversion = float('-inf')
            best_price = None
            for price in self.get_prices():
                conversion = self.configuration["price_curve"][price]
                if conversion > best_conversion:
                    best_conversion = conversion
                    best_price = price
            return best_price
        elif scenario == "advertising":
            #Finding the bid with the most clicks
            best_clicks = float('-inf')
            best_costs = float('inf')
            best_bid_clicks = None
            all_bids = self.get_bids()
            for bid in all_bids:
                clicks = self.configuration["click_curve"][bid]
                if clicks > best_clicks:
                    best_bid_clicks = bid
            for bid in all_bids:
                costs = self.configuration["cost_curve"][bid]
                if costs < best_costs:
                    best_bid_costs = bid
            return (best_bid_clicks, best_bid_costs)
    
    
    def find_best_price_and_reward(self, best_bids: dict): 
        '''
        This function looks for the best key->key-value pair within the best bids dictionary
        We return the price with the highest reward
        It is used in the pull_best_price function
        Returns the best price and the reward
        Best price is the first index [0]
        Best Reward is the second index [1]
        '''
        max_reward = float('-inf')
        max_reward_tuple = None

        for bid, prices in best_bids.items():
            for price, reward in prices.items():
                if reward > max_reward:
                    max_reward = reward
                    max_reward_tuple = (reward, price, bid)

        return max_reward_tuple
    
    def find_best_bid(self, price:int):
        '''
        This function iterates through the best bids dictionary and returns the bid with the highest reward given a specific (mostly suboptimal) price that was returned by UCB or TS
        It returns a tuple containing the best bid and the reward for that price
        '''
        max_reward = float('-inf')
        max_bid = None
        for bid, reward_dict in self.best_bids.items():
            if price in reward_dict:
                #print(reward_dict)
                reward = reward_dict[price]
                if reward > max_reward:
                    max_reward = reward
                    best_bid = bid

        return best_bid, max_reward
    
    def pull_arm(self, parameters: dict, objective_function, used_algorithm: str) -> int:
        '''
        Serrve no use, but needed to be implemented because abstract class
        '''
        return super().pull_arm(parameters, objective_function, used_algorithm)
    
    def get_target_value(self, configuration, reward):
        '''
        Serve no use, but needed to be implemented because abstract class
        '''
        return super().get_target_value(configuration, reward)
    
    def update_parameters(self, reward: list, pull_arm: int) -> dict:
        '''
        Serve no use, but needed to be implemented because abstract class
        '''
        return super().update_parameters(reward, pull_arm)
    def read_configuration(self, user_class: str) -> dict:
        return super().read_configuration(user_class)
    

class mock_process_advertisment(abc_process):
    def __init__(self, n_arms: int, user_class: str, used_algorithm: str, learning_cost: bool): 
        super().__init__(n_arms, user_class)
        self.used_algorithm = used_algorithm    
        self.learning_cost = learning_cost
        self.rewards_per_arm = x = [[] for arm in range(self.n_arms)]
        self.arm_counter = [0]*self.n_arms
        self.collected_rewards = []
        self.best_prices = dict()
        self.arms = list(range(n_arms))
        self.confidence = np.array([np.inf]*self.n_arms) #  Confidence is needed for GP-UCB
        assert n_arms == 100 #should be size 100
        #GP-Initialization
        self.means = np.zeros(self.n_arms)

        self.sigmas = np.ones(self.n_arms)*10
        self.pulled_arms  = [] # store arms pulled at each round
        alpha = 10.0 # value of sd of the noise
        # initialize kernel and its scale factor & param ranges
        kernel = C(1.0, (1e-3, 1e3)) * RBF(1.0, (1e-3, 1e3))
        self.gp = GaussianProcessRegressor(kernel = kernel, 
                                           alpha = alpha**2,  # variance
                                           normalize_y = True, # normalization
                                           n_restarts_optimizer = 9) # how many times to optimize hyperpars


    def get_name(self) -> str:
        return "Advertising"
    def pull_arm(self) -> int:
        '''
        If Learning CLICKS: Returns the bid with the highest expected value (number of clicks) <=> Highest => Highest confidence bound
        If Learning COST: Returns the bid with the lowest expected value (cost) <=> Lowest => Lowest confidence bound
        '''
        if self.used_algorithm == "GP-UCB":
            if self.learning_cost:
                sampled_values = np.random.normal(self.means, self.sigmas) - self.confidence
                arm = np.argmin(sampled_values)# pull the bid with minimum expected value if we learn the cost
                self.arm_counter[arm] += 1
                return arm
                
            else: 
                sampled_values = np.random.normal(self.means, self.sigmas) + self.confidence
                arm = np.argmax(sampled_values)# pull the bid with maximum expected value
                self.arm_counter[arm] += 1
                return arm
        
    
        elif self.used_algorithm == "GP-TS":
            if self.learning_cost:
                sampled_values = np.random.normal(self.means, self.sigmas) 
                arm = np.argmin(sampled_values)# pull the bid with minimum expected value if we learn the cost
                self.arm_counter[arm] += 1
                return arm
            else:
                sampled_values = np.random.normal(self.means, self.sigmas) 
                arm = np.argmax(sampled_values)# pull the bid with maximum expected value
                self.arm_counter[arm] += 1
                return arm

    def return_arm_for_bid(self, bid: float) -> int:
        '''
        Returns the arm for a specific bid
        '''
        return list(self.bids).index(bid)
    def return_bid_for_arm(self, arm: int) -> int:
        '''
        Returns the bid for a specific arm
        '''
        return self.bids[arm]
    
    def calculate_mean_clicks(self, click_curve: dict):
        click_values = click_curve.values()
        if len(click_values) == 0:
            return None  # Handle the case of an empty dictionary
        mean = sum(click_values) / len(click_values)
        return mean
    
    def convert_to_flat_list(self, lst):
        '''
        Helper function to flatten the rewards per arm list to be used in the Gaussian process
        '''
        return [sublst[0] if sublst else 0 for sublst in lst]


    def initialize(self):
        '''
        Utility class to initialize the Cofidence intervals
        We retrieve one reward per arm according to UCB1
        Note: This is only needed if UCB is used
        '''
        print(f"Initializing| {self.used_algorithm} bandit")
        for i in range(0, self.n_arms):
            
            reward = self.get_target_value(i)
            self.update(i, reward)
        print(f"Finished initializing| {self.used_algorithm} bandit")

    def get_target_value(self, pulled_arm: int, user_class: str = None) -> float:
        '''
        If we learn Clicks: This function returns the expected number of clicks given an arm
        If we learn Cost: This function returns the expected cost given an arm
        '''
        if user_class != None:
            if self.learning_cost:
                return self.configuration[user_class]["cost_curve"][self.bids[pulled_arm]]
        if self.learning_cost:
            return self.configuration["cost_curve"][self.bids[pulled_arm]]
        else:
            return self.configuration["click_curve"][self.bids[pulled_arm]]
    
    def update_parameters(self, reward: float, pull_arm: int) -> dict:
        '''
        This function updates collects the reward 
        In case of GP-UCB, we need to update the confidence bounds
        '''
        self.t += 1 # at each round, increment time
        self.rewards_per_arm[pull_arm].append(reward) #recall: python list
        self.collected_rewards.append(reward)
        if self.used_algorithm == "GP-UCB":
            #This is necessary in case GP-UCB is used
            self.confidence[pull_arm] = np.sqrt(2*np.log(self.t)/len(self.rewards_per_arm[pull_arm]))
        self.pulled_arms.append(pull_arm)
    
    def update_model(self):
        '''
        This function updates the Gaussian Process model
        X: The arms pulled
        Y: The rewards collected
        Note: That both arrays keep on growing over each step and slows down the fitting process significantly
        Therefore, we only use the last 400 steps to fit the model, otherwise reaching a time horizon of 365 would not be feasible
        GP's have a big O of O(n^3) and therefore, we need to reduce the number of steps to fit the model
        '''
        # training inputs
        if len(self.pulled_arms) > 400:
            cut_off_pulled_arms = self.pulled_arms[-400:]
            cut_off_collected_rewards = self.collected_rewards[-400:]
            x = np.atleast_2d(cut_off_pulled_arms).T
            y = cut_off_collected_rewards
        else:
            x = np.atleast_2d(self.pulled_arms).T
            # targets
            y = self.collected_rewards
        self.gp.fit(x,y) # fit gp
        # update means & sigmas with new predictions
        self.means, self.sigmas = self.gp.predict(np.atleast_2d(self.arms).T, return_std = True)
        self.sigmas = np.maximum(self.sigmas, 1e-2)

    def update(self, pulled_arm, reward, UPDATE_MODEL = True):
        '''
        Function to encapsulate the update of the parameters and the model
        '''
        self.update_parameters(reward, pulled_arm)
        if UPDATE_MODEL:
            self.update_model()

    #Implement all abstract methods -_-
    def read_configuration(self, user_class: str) -> dict:
        return super().read_configuration(user_class)

