from abstract import mock_process, clairvoyant_process
from util import colours, plot_helper
import matplotlib.pyplot as plt
import numpy as np
import time 
#Utilities
colour = colours()
harry_plotter = plot_helper()
from collections import Counter

#Hyperparameters
N_ARMS = 5 #We learn pricing
USED_ALGORITHM = "UCB"
Mock_UCB = mock_process(N_ARMS, USED_ALGORITHM, "C1")
TIME_HORIZON = 365
N_EXPERIMENTS = 100
UCB_rewards_per_experiment = []
UCB_cumulative_rewards_per_experiment = []
UCB_objective_rewards_per_experiment = []
UCB_objective_cumulative_rewards_per_experiment = []
UCB_objective_regret_per_experiment = []

USED_ALGORITHM = "TS"
Mock_TS = mock_process(N_ARMS, USED_ALGORITHM, "C1")
TS_rewards_per_experiment = []
TS_objective_rewards_per_experiment = []
TS_cumulative_rewards_per_experiment = []
TS_objective_cumulative_rewards_per_experiment = []
TS_objective_regret_per_experiment = []

CV_rewards_per_experiment = []
CV_cumulative_rewards_per_experiment = []
Mock_CV = clairvoyant_process(N_ARMS, "C1")
clairvoyant_arm_and_reward = Mock_CV.pull_best_arm()


#Measuring the runtime 
start_time = time.time()

#Initialize UCB => Pull every arm once
Mock_UCB.initialize()
for t in range(0, TIME_HORIZON):
    print(f"Time {t}")
    for e in range(0, N_EXPERIMENTS):
        # THOMPSON SAMPLING
        predicted_value_TS = Mock_TS.pull_arm() # estimated by MAB

        predicted_value_as_price_TS = Mock_TS.return_price_for_arm(predicted_value_TS)

        # UCB
         # first, predict the reward with MAB
        predicted_value_UCB = Mock_UCB.pull_arm() # estimated by MAB
        predicted_value_as_price_UCB = Mock_UCB.return_price_for_arm(predicted_value_UCB)
        # then, get the real (randomized reward for comparison)
        target_value_UCB = Mock_UCB.get_target_value(predicted_value_as_price_UCB) # real value

        # CLAIRVOYANT
        Objective_TS_Reward = Mock_CV.find_best_bid(predicted_value_as_price_TS)[1]
        Objective_UCB_Reward = Mock_CV.find_best_bid(predicted_value_as_price_UCB)[1]

        clairvoyant_arm = clairvoyant_arm_and_reward[0]
        clairvoyant_reward = clairvoyant_arm_and_reward[1]

        Objective_TS_Regret = clairvoyant_reward - Objective_TS_Reward
        Objective_UCB_Regret = clairvoyant_reward - Objective_UCB_Reward
        
        # then, get the real (randomized reward for comparison)
        target_value_TS = Mock_TS.get_target_value(predicted_value_as_price_TS) # real value
        # update parameters for UCB
        parameters_UCB = Mock_UCB.update_parameters(target_value_UCB, predicted_value_UCB)
        # update parameters for TS
        parameters_TS = Mock_TS.update_parameters(target_value_TS, predicted_value_TS)


        # Append the rewards to the list for plotting purposes
        CV_rewards_per_experiment.append(clairvoyant_reward)
        TS_rewards_per_experiment.append(target_value_TS)
        TS_objective_rewards_per_experiment.append(Objective_TS_Reward)
        TS_cumulative_rewards_per_experiment.append(sum(TS_rewards_per_experiment))
        TS_objective_cumulative_rewards_per_experiment.append(sum(TS_objective_rewards_per_experiment))
        UCB_rewards_per_experiment.append(target_value_UCB)
        UCB_cumulative_rewards_per_experiment.append(sum(UCB_rewards_per_experiment))
        UCB_objective_rewards_per_experiment.append(Objective_UCB_Reward)
        UCB_objective_cumulative_rewards_per_experiment.append(sum(UCB_objective_rewards_per_experiment))
        UCB_objective_regret_per_experiment.append(Objective_UCB_Regret)
        TS_objective_regret_per_experiment.append(Objective_TS_Regret)

end_time = time.time()

print(f"Time elapsed: {end_time - start_time}")
print(colour.greenify("UCB rewards:"))
print(sum(UCB_objective_rewards_per_experiment))
print(colour.greenify("TS rewards:"))
print(sum(TS_objective_rewards_per_experiment))
print(colour.greenify("CV rewards:"))
print(sum(CV_rewards_per_experiment))

#Calculating the mean at each step to be plotted

#For UCB
# Calculate the cumulative sum of the array
cumulative_sum_UCB = np.cumsum(UCB_objective_rewards_per_experiment)
# Calculate the mean at each index
mean_values_UCB = cumulative_sum_UCB / np.arange(1, len(cumulative_sum_UCB) + 1)
# Calculate the standard deviation at each index
std_values_UCB = np.std(cumulative_sum_UCB[:len(mean_values_UCB)])

#For TS
# Calculate the cumulative sum of the array
cumulative_sum_TS = np.cumsum(TS_objective_rewards_per_experiment)
# Calculate the mean at each index
mean_values_TS = cumulative_sum_TS / np.arange(1, len(cumulative_sum_TS) + 1)
# Calculate the standard deviation at each index
std_values_TS = np.std(cumulative_sum_TS[:len(mean_values_TS)])

#For CV
# Calculate the cumulative sum of the array
cumulative_sum_CV = np.cumsum(CV_rewards_per_experiment)
# Calculate the mean at each index
mean_values_CV = cumulative_sum_CV / np.arange(1, len(cumulative_sum_CV) + 1)
# Calculate the standard deviation at each index
std_values_CV = np.std(cumulative_sum_CV[:len(mean_values_CV)])


#Plot Average reward over time
plt.figure("Average Reward over time")

plt.ylabel("Average Reward")

plt.xlabel("Time")

plt.plot(mean_values_TS, 'cyan')

plt.plot(mean_values_UCB, 'green')

plt.plot(mean_values_CV, 'blue')

plt.legend(["Thompson Sampling", "UCB1", "Clairvoyant"])

plt.savefig('graphs/step_1/Average_Reward_over_time.png')

plt.show()

#Plotting each reward over time for TS including its respective standard deviation
moved_mean_TS, moved_standard_TS = harry_plotter.calculate_moving_average(TS_objective_rewards_per_experiment, N_EXPERIMENTS)
# Generating values along the x axis
x_axis = np.arange(1, len(moved_mean_TS) + 1)

plt.figure("TS Average Reward incl. Standard Deviation")

plt.plot(x_axis, moved_mean_TS, label = "TS Average reward",color="cyan")

plt.errorbar(x_axis, moved_mean_TS, yerr = moved_standard_TS, color='grey', label = "TS Standard Deviation", linestyle = 'None', marker = 'o')

plt.xlabel("Time Steps")

plt.ylabel("Reward")

plt.title("TS Average Reward incl. Standard Deviation")

plt.legend()

plt.savefig('graphs/step_1/TS_Average_Reward_incl_Standard_Deviation.png')

plt.show()

#Plotting each reward over time for TS including its respective standard deviation
moved_mean_UCB, moved_standard_UCB = harry_plotter.calculate_moving_average(UCB_objective_rewards_per_experiment, N_EXPERIMENTS)
# Generating values along the x axis
x_axis = np.arange(1, len(moved_mean_UCB) + 1)

plt.figure(2)

plt.plot(x_axis, moved_mean_UCB, label = "UCB Average reward", color="green")

plt.errorbar(x_axis, moved_mean_UCB, yerr = moved_standard_UCB, label = "UCB Standard Deviation", linestyle = 'None', marker = 'o', color='grey')

plt.xlabel("Experiments")

plt.ylabel("Reward")

plt.title("UCB Average Reward incl. Standard Deviation")

plt.legend()

plt.savefig('graphs/step_1/UCB_Average_Reward_incl_Standard_Deviation.png')

plt.show()





reward_per_time_step_TS = harry_plotter.calculate_experiment_sums(TS_objective_rewards_per_experiment, N_EXPERIMENTS)
reward_per_time_step_UCB = harry_plotter.calculate_experiment_sums(UCB_objective_rewards_per_experiment, N_EXPERIMENTS)
reward_per_time_step_CV = harry_plotter.calculate_experiment_sums(CV_rewards_per_experiment, N_EXPERIMENTS)

#Plot instantaneous reward

plt.figure(3)

plt.ylabel("Instantaneous Reward per Time Step")

plt.xlabel("Time")

plt.plot(reward_per_time_step_TS, 'cyan')

plt.plot(reward_per_time_step_UCB, 'green')

plt.plot(reward_per_time_step_CV, 'blue')

""" plt.plot(np.mean(np.array([UCB_objective_rewards_per_experiment]), axis = 0), 'g')

plt.plot(np.mean(np.array([CV_rewards_per_experiment]), axis = 0), 'b') """

plt.legend(["Thompson Sampling", "UCB1", "Clairvoyant"])

plt.savefig('graphs/step_1/Instantaneous_Reward_per_Time_Step.png')

plt.show()

cumulative_reward_per_time_step_TS = harry_plotter.calculate_accumulated_experiment_sums(TS_objective_rewards_per_experiment, N_EXPERIMENTS)
cumulative_reward_per_time_step_UCB = harry_plotter.calculate_accumulated_experiment_sums(UCB_objective_rewards_per_experiment, N_EXPERIMENTS)
cumulative_reward_per_time_step_CV = harry_plotter.calculate_accumulated_experiment_sums(CV_rewards_per_experiment, N_EXPERIMENTS)

#Plot instantaneous reward

plt.figure(4)

plt.ylabel("Cumulative Reward per Time Step")

plt.xlabel("Time")

plt.plot(cumulative_reward_per_time_step_TS, 'cyan')

plt.plot(cumulative_reward_per_time_step_UCB, 'green')

plt.plot(cumulative_reward_per_time_step_CV, 'blue')

""" plt.plot(np.mean(np.array([UCB_objective_rewards_per_experiment]), axis = 0), 'g')

plt.plot(np.mean(np.array([CV_rewards_per_experiment]), axis = 0), 'b') """

plt.legend(["Thompson Sampling", "UCB1", "Clairvoyant"])

plt.savefig('graphs/step_1/Cumulative_Reward_per_Time_Step.png')

plt.show()

regret_per_time_step_TS = harry_plotter.calculate_experiment_sums(TS_objective_regret_per_experiment, N_EXPERIMENTS)
regret_per_time_step_UCB = harry_plotter.calculate_experiment_sums(UCB_objective_regret_per_experiment, N_EXPERIMENTS)

#Plot instantaneous regret

plt.figure(5)

plt.ylabel("Instantaneous Regret per Time Step")

plt.xlabel("Time")

plt.plot(regret_per_time_step_TS, 'cyan')

plt.plot(regret_per_time_step_UCB, 'green')

plt.legend(["Thompson Sampling", "UCB1"])

plt.savefig('graphs/step_1/Instantaneous_Regret_per_Time_Step.png')
plt.show()

cumulative_regret_per_time_step_TS = harry_plotter.calculate_accumulated_experiment_sums(TS_objective_regret_per_experiment, N_EXPERIMENTS)
cumulative_regret_per_time_step_UCB = harry_plotter.calculate_accumulated_experiment_sums(UCB_objective_regret_per_experiment, N_EXPERIMENTS)

#Plot cumulative regret

plt.figure(6)

plt.ylabel("Cumulative Regret per Time Step")

plt.xlabel("Time")

plt.plot(cumulative_regret_per_time_step_TS, 'cyan')

plt.plot(cumulative_regret_per_time_step_UCB, 'green')

plt.legend(["Thompson Sampling", "UCB1"])

plt.savefig('graphs/step_1/Cumulative_Regret_per_Time_Step.png')

plt.show()



#Plot cumulative reward

plt.figure(7)

plt.ylabel("Cumulative Reward per Experiment")

plt.xlabel("Experiment")

plt.plot(np.cumsum(np.mean(np.array([TS_objective_rewards_per_experiment]), axis = 0)), 'cyan')

plt.plot(np.cumsum(np.mean(np.array([UCB_objective_rewards_per_experiment]), axis = 0)), 'green')

plt.plot(np.cumsum(np.mean(np.array([CV_rewards_per_experiment]), axis = 0)), 'blue')

plt.legend(["Thompson Sampling", "UCB1", "Clairvoyant"])

plt.savefig('graphs/step_1/Cumulative_Reward_per_Experiment.png')

plt.show()

