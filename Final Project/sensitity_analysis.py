
#Sensitivity Analysis for UCB-SW
#Input Parameters 
#We vary our window parameter in steps of 10 in between +-20 of the season duration
from window import simulate_window
from change_detect import simulate_change_detection
import matplotlib.pyplot as plt

#Flip this boolean to simulate windows or change detection
SIMULATE_WINDOWS = True

if SIMULATE_WINDOWS:
    WINDOW_SIZES = [9000, 10000, 11000, 12100, 13100, 14100]

    mean_rewards_per_experiment = []
    total_rewards = []
    moving_Averages = []
    standard_deviations = []

    for window in WINDOW_SIZES:
        results = simulate_window(window)
        mean_rewards_per_experiment.append(results[0])
        moving_Averages.append(results[1])
        standard_deviations.append(results[2])
        total_rewards.append(results[3])
        print(f"Using a window size of {window} we get a mean reward per experiment of {results[0]}")
        print(f"Using a window size of {window} we get a total reward of {results[3]}")

    WINDOW_SIZES_STR = [str(i) for i in WINDOW_SIZES]
    #Plotting the mean rewards per experiment in a bar chart
    plt.bar(WINDOW_SIZES_STR, mean_rewards_per_experiment, color="black")
    plt.xlabel("Window Size")
    plt.ylabel("Mean Reward per Experiment")
    plt.title("Mean Reward per Experiment for different Window Sizes")
    plt.savefig("graphs/step_5/sensitivity_analysis/Mean Reward per Experiment for different Window Sizes")
    plt.show()
    #Plotting the total rewards in a bar chart
    plt.bar(WINDOW_SIZES_STR, total_rewards, color="black")
    plt.xlabel("Window Size")
    plt.ylabel("Total Reward")
    plt.title("Total Reward for different Window Sizes")
    plt.savefig("graphs/step_5/sensitivity_analysis/Total Reward for different Window Sizes")
    plt.show()
    #Plotting the moving average for each window size
    plt.plot(moving_Averages[0], color="red")
    plt.plot(moving_Averages[1], color="blue")
    plt.plot(moving_Averages[2], color="green")
    plt.plot(moving_Averages[3], color="yellow")
    plt.plot(moving_Averages[4], color="purple")
    plt.plot(moving_Averages[5], color="orange")
    plt.xlabel("Experiment")
    plt.ylabel("Moving Average")
    plt.title("Moving Average for different Window Sizes")
    plt.legend(["Window Size 9000", "Window Size 10000", "Window Size 11000", "Window Size 12100", "Window Size 13100", "Window Size 14100"])
    plt.savefig('graphs/step_5/sensitivity_analysis/Moving Average for different Window Sizes')
    plt.show()
    #Plotting the standard deviation for each window size
    plt.plot(standard_deviations[0], color="red")
    plt.plot(standard_deviations[1], color="blue")
    plt.plot(standard_deviations[2], color="green")
    plt.plot(standard_deviations[3], color="yellow")
    plt.plot(standard_deviations[4], color="purple")
    plt.plot(standard_deviations[5], color="orange")
    
    plt.xlabel("Experiment")
    plt.ylabel("Standard Deviation")
    plt.title("Standard Deviation for different Window Sizes")
    plt.legend(["Window Size 9000", "Window Size 10000", "Window Size 11000", "Window Size 12100", "Window Size 13100", "Window Size 14100"])
    plt.savefig('graphs/step_5/sensitivity_analysis/Standard Deviation for different Window Sizes')
    plt.show()
else:
    TAUS = [1, 2, 3, 4, 5]

    mean_rewards_per_experiment = []
    total_rewards = []
    moving_Averages = []
    standard_deviations = []

    for tau in TAUS:
        results = simulate_change_detection(tau)
        mean_rewards_per_experiment.append(results[0])
        moving_Averages.append(results[1])
        standard_deviations.append(results[2])
        total_rewards.append(results[3])
        print(f"Using a tau of {tau} we get a mean reward per experiment of {results[0]}")
        print(f"Using a window size of {tau} we get a total reward of {results[3]}")

    TAUS_STR = [str(i) for i in TAUS]
    #Plotting the mean rewards per experiment in a bar chart
    plt.bar(TAUS_STR, mean_rewards_per_experiment, color="black")
    plt.xlabel("Tau")
    plt.ylabel("Mean Reward per Experiment")
    plt.title("Mean Reward per Experiment for different Taus")
    plt.savefig("graphs/step_5/sensitivity_analysis/Mean Reward per Experiment for different Taus")
    plt.show()
    #Plotting the total rewards in a bar chart
    plt.bar(TAUS_STR, total_rewards, color="black")
    plt.xlabel("Tau")
    plt.ylabel("Total Reward")
    plt.title("Total Reward for different Taus")
    plt.savefig("graphs/step_5/sensitivity_analysis/Total Reward for different Taus")
    plt.show()
    #Plotting the moving average for each window size
    plt.plot(moving_Averages[0], color="red")
    plt.plot(moving_Averages[1], color="blue")
    plt.plot(moving_Averages[2], color="green")
    plt.plot(moving_Averages[3], color="orange")
    plt.plot(moving_Averages[4], color="purple")


    plt.xlabel("Time Step")
    plt.ylabel("Moving Average Reward")
    plt.title("Moving Average Reward for different Taus")
    plt.legend(["Tau 1", "Tau 2", "Tau 3", "Tau 4", "Tau 5"])
    plt.savefig("graphs/step_5/sensitivity_analysis/Moving Average Reward for different Taus")
    plt.show()
    #Plotting the standard deviation for each window size
    plt.plot(standard_deviations[0], color="red")
    plt.plot(standard_deviations[1], color="blue")
    plt.plot(standard_deviations[2], color="green")
    plt.plot(standard_deviations[3], color="orange")
    plt.plot(standard_deviations[4], color="purple")

    plt.xlabel("Time Step")
    plt.ylabel("Standard Deviation")
    plt.title("Moving Standard Deviation for different Taus")
    plt.legend(["Tau 1", "Tau 2", "Tau 3", "Tau 4", "Tau 5"])
    plt.savefig("graphs/step_5/sensitivity_analysis/Moving Standard Deviation for different Taus")
    plt.show()
