
import time
import datetime
import os
import sys
import shutil
import socket
from tkinter import *
import matplotlib.pyplot as plt
import mpl_toolkits
from mpl_toolkits.mplot3d import Axes3D
from mpl_toolkits.mplot3d.art3d import Poly3DCollection
from PIL import Image
import numpy as np

class colours:
    def __init__(self):
        self.Green = '\033[32m'
        self.White = '\033[37m'
        self.Red = '\033[31m'
        self.Yellow = '\033[33m'
        self.Blue = '\033[34m'
        self.Cyan = '\033[36m'
        self.Purple = '\033[35m'

    def greenify(self, text_to_greenify):
        '''
        Returned den string mit den angehängten Stringrepräsentationen die eine grüne Darstellung in der CLI ermöglichen
        '''
        return self.Green + text_to_greenify + self.White     

    def redify(self, text_to_redify):
        '''
        Returned den string mit den angehängten Stringrepräsentationen die eine rote Darstellung in der CLI ermöglichen
        '''
        return self.Red + text_to_redify + self.White  
    
    def purplify(self, text_to_purplify):
        '''
        Returned den string mit den angehängten Stringrepräsentationen die eine lila Darstellung in der CLI ermöglichen
        '''
        return self.Purple + text_to_purplify + self.White  

    def yellowfy(self, text_to_yellowfy):
        '''
        Returned den string mit den angehängten Stringrepräsentationen die eine gelbe Darstellung in der CLI ermöglichen
        '''
        return self.Yellow + text_to_yellowfy + self.White  

    def cyanify(self, text_to_cyanify):
        '''
        Returned den string mit den angehängten Stringrepräsentationen die eine hellblaue Darstellung in der CLI ermöglichen
        '''
        return self.Cyan + text_to_cyanify + self.White
class plot_helper:
    def __init__(self) -> None:
        self.bids = self.get_bids()

    def normalize_arm_counts(self, arm_counts:list): 
        pull_counts = np.array(arm_counts)

        # Compute proportion of times each arm has been pulled
        pull_proportions = pull_counts / np.sum(pull_counts)
        return pull_proportions


    def calculate_average_reward(self, rewards, bids):
        arm_rewards = {}  # Dictionary to store rewards per arm
        arm_counts = {}  # Dictionary to store counts per arm

        for reward, bid in zip(rewards, bids):
            if bid not in arm_rewards:
                arm_rewards[bid] = reward
                arm_counts[bid] = 1
            else:
                arm_rewards[bid] += reward
                arm_counts[bid] += 1

        average_rewards = {}
        for bid in arm_rewards:
            average_rewards[bid] = arm_rewards[bid] / arm_counts[bid]

        return average_rewards
    def fill_empty_values(self, input_dict, range_limit):
        '''
        This function is needed in case that some arms have not been pulled yet
        '''
        new_dict = {}
        for i in range(range_limit):
            new_dict[i] = input_dict.get(i, 0)
        return new_dict

    def calculate_experiment_sums(self, rewards, num_experiments):
        '''
        Function receives the number of experiments and the rewards and returns the sums for each experiment as a numpy array
        '''
        # Ensure rewards is a numpy array
        rewards = np.array(rewards)

        # Check that rewards length is a multiple of num_experiments
        assert rewards.shape[0] % num_experiments == 0, 'Rewards length should be a multiple of num_experiments'

        # Reshape rewards into (num_timesteps, num_experiments) shape
        reshaped_rewards = rewards.reshape(-1, num_experiments)

        # Compute sums
        sums = np.sum(reshaped_rewards, axis=1)

        return sums
    
    import numpy as np

    def calculate_accumulated_experiment_sums(self, rewards, num_experiments):
        # Ensure rewards is a numpy array
        rewards = np.array(rewards)

        # Check that rewards length is a multiple of num_experiments
        assert rewards.shape[0] % num_experiments == 0, 'Rewards length should be a multiple of num_experiments'

        # Reshape rewards into (num_timesteps, num_experiments) shape
        reshaped_rewards = rewards.reshape(-1, num_experiments)

        # Compute sums
        sums = np.sum(reshaped_rewards, axis=1)

        # Compute accumulated sums
        accumulated_sums = np.cumsum(sums)

        return accumulated_sums


    def calculate_moving_average(self, rewards, num_experiments):
        '''
        Function receives the number of experiments and the rewards and returns the moving average and the standard deviation
        - [0] => moving average
        - [1] => moving standard deviation
        '''
        # Ensure data is a numpy array
        rewards = np.array(rewards)

        # Check that data length is a multiple of num_experiments
        assert rewards.shape[0] % num_experiments == 0, 'Data length should be a multiple of num_experiments'

        # Reshape data into (num_timesteps, num_experiments) shape
        reshaped_data = rewards.reshape(-1, num_experiments)

        # Compute moving average and standard deviation
        moving_avg = np.mean(reshaped_data, axis=1)
        moving_std = np.std(reshaped_data, axis=1)

        return moving_avg, moving_std

    

    
    def get_bids(self):
        # Just needed to map bids and arms
        min_bid = 0.01
        max_bid = 0.1
        bids = np.linspace(min_bid, max_bid, num=100)
        return bids
    
    def return_bid_for_arm(self, arm: int) -> int:
        '''
        Returns the bid for a specific arm
        '''
        return self.bids[arm]
    
    def return_arm_for_bid(self, bid: float) -> int:
        '''
        Returns the arm for a specific bid
        '''
        return list(self.bids).index(bid)
    
    def convert_arm_list(self, pulled_arms: list) -> list:
        '''
        Converts a list of pulled arms to a list of bids
        '''
        bids = []
        for arm in pulled_arms:
            bids.append(self.return_bid_for_arm(arm))
        return bids
    
class context_helper():
    def __init__(self) -> None:
        self.FEATURE_SET_CLASSES = {"C1": (0,0), "C2": (0,1), "C3": [(1,0), (1,1)]}
    def get_class_for_feature_set(self, feature_set: tuple) -> list:
        '''
        Returns the class for a specific feature set, meaning we return the key of FEATURE_SET_CLASSES if the value is equal to the feature_set
        '''
        for key, value in self.FEATURE_SET_CLASSES.items():
            if type(value) == list:
                for val in value:
                    if val == feature_set:
                        return key
            if value == feature_set:
                return key
            
    def get_feature_set_for_class(self, class_: str) -> list:
        '''
        Returns the feature set for a specific class, meaning we return the value of FEATURE_SET_CLASSES if the key is equal to the class
        NOTE: if class is C3 you get a list containiing two tuples, because two feature sets fall on one class
        '''
        for key, value in self.FEATURE_SET_CLASSES.items():
            if key == class_:
                return value

class feature_helper():
    def __init__(self) -> None:
        self.FEATURE_SET_CLASSES = {"1": (0,0), "2": (0,1), "3": (1, 0), "4": (1,1)}
    def get_number_for_feature_set(self, feature_set: tuple) -> list:
        '''
        Returns the class for a specific feature set, meaning we return the key of FEATURE_SET_CLASSES if the value is equal to the feature_set
        '''
        for key, value in self.FEATURE_SET_CLASSES.items():
            if value == feature_set:
                return key
            
    def get_feature_set_for_number(self, class_: str) -> list:
        '''
        Returns the feature set for a specific class, meaning we return the value of FEATURE_SET_CLASSES if the key is equal to the class
        NOTE: if class is C3 you get a list containiing two tuples, because two feature sets fall on one class
        '''
        for key, value in self.FEATURE_SET_CLASSES.items():
            if key == class_:
                return value
"""
TESTING UTIL 
FEATURE_SETS  = [(0,0), (0,1), (1,0), (1,1)]

contextualizer = context_helper()
for feature_set in FEATURE_SETS:
    print(f"Feature set: {feature_set} has class: {contextualizer.get_class_for_feature_set(feature_set)}") """

""" ABC = [10, 20, 10, 40]
No_of_experiments = 2

plotter = plot_helper()
moving_average, std_values = plotter.calculate_moving_average(No_of_experiments, ABC)
print(moving_average)
print(std_values) """
