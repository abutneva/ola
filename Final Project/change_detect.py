from abstract import mock_process, clairvoyant_process
from util import colours, plot_helper
import matplotlib.pyplot as plt
import numpy as np
#Utilities
colour = colours()
harry_plotter = plot_helper()
from collections import Counter

def calculate_mean_total_reward_per_experiment(rewards_per_experiment: list):
    mean_total_reward_per_experiment = []
    for experiment in rewards_per_experiment:
        mean_total_reward_per_experiment.append(np.mean(experiment))
    return mean_total_reward_per_experiment

def simulate_change_detection(tau):
    '''
    Function returns:
    - the mean reward per experiment [0]
    - the moving averages of the reward per time step [1]
    - the standard deviation of the reward per time step [2]
    - the total reward of the whole simulation [3]
    '''
    #Hyperparameters
    N_ARMS = 5 #We learn pricing

    TIME_HORIZON = 365
    N_EXPERIMENTS = 100
    SEASONS_CHANGE_EVERY = 121
    SEASONS = ["beachbody", "new year resolution", "lazy"]



    USED_ALGORITHM = "CD-UCB"
    Mock_UCB_CD = mock_process(N_ARMS, USED_ALGORITHM, "C1")
    #CD_UCB
    СD_UCB_rewards_per_experiment = []
    СD_UCB_rewards_per_experiment
    CD_UCB_cumulative_rewards_per_experiment = []
    CD_UCB_objective_rewards_per_experiment = []
    CD_UCB_objective_cumulative_rewards_per_experiment = []
    CD_UCB_objective_regret_per_experiment = []
    TAU = tau


    # Clairvoyant
    CV_rewards_per_experiment = []
    CV_cumulative_rewards_per_experiment = []
    Mock_CV = clairvoyant_process(N_ARMS, "C1")
    clairvoyant_arm_and_reward = Mock_CV.pull_best_arm()


    Mock_UCB_CD.initialize()
    counter = 0
    for t in range(0, TIME_HORIZON):

        if t % SEASONS_CHANGE_EVERY == 0:
            SEASON = SEASONS[counter % len(SEASONS)]
            counter += 1

            Mock_UCB_CD.change_configuration(SEASON)
            Mock_CV.change_configuration(SEASON)
            clairvoyant_arm_and_reward = Mock_CV.pull_best_arm()
            clairvoyant_arm = clairvoyant_arm_and_reward[0]
            clairvoyant_reward = clairvoyant_arm_and_reward[1]
        for e in range(0, N_EXPERIMENTS):

            # UCB_CD
             # first, predict the reward with MAB
            predicted_value_CD_UCB = Mock_UCB_CD.pull_arm() # estimated by MAB
            #print("predicted_value_CD_UCB", predicted_value_CD_UCB)
            predicted_value_as_price_CD_UCB = Mock_UCB_CD.return_price_for_arm(predicted_value_CD_UCB)
            # then, get the real (randomized reward for comparison)
            target_value_CD_UCB = Mock_UCB_CD.get_target_value(predicted_value_as_price_CD_UCB) # real value

    
            Objective_UCB_CD_Reward = Mock_CV.find_best_bid(predicted_value_as_price_CD_UCB)[1]



            Objective_UCB_CD_Regret = clairvoyant_reward - Objective_UCB_CD_Reward

            # update parameters for UCB_CD
            parameters_UCB_CD = Mock_UCB_CD.update_parameters(target_value_CD_UCB, predicted_value_CD_UCB, tau=TAU)


            # Append the rewards to the list for plotting purposes
            # Clairvoyant
            CV_rewards_per_experiment.append(clairvoyant_reward)

            #CD-UCB
            СD_UCB_rewards_per_experiment.append(target_value_CD_UCB)
            CD_UCB_cumulative_rewards_per_experiment.append(sum(СD_UCB_rewards_per_experiment))
            CD_UCB_objective_rewards_per_experiment.append(Objective_UCB_CD_Reward)
            CD_UCB_objective_cumulative_rewards_per_experiment.append(sum(CD_UCB_objective_rewards_per_experiment))
            CD_UCB_objective_regret_per_experiment.append(Objective_UCB_CD_Regret)

    moved_mean_UCB_SW, moved_standard_UCB_SW = harry_plotter.calculate_moving_average(CD_UCB_objective_rewards_per_experiment, N_EXPERIMENTS)

    print(f"Simulation with tau {colour.cyanify(str(tau))} is done")
    mean_per_experiment = calculate_mean_total_reward_per_experiment([CD_UCB_objective_rewards_per_experiment])
    return (mean_per_experiment[0], moved_mean_UCB_SW, moved_standard_UCB_SW, sum(CD_UCB_objective_rewards_per_experiment))