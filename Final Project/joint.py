from abstract import clairvoyant_process, mock_process_advertisment, mock_process
from util import colours, plot_helper
import matplotlib.pyplot as plt
import matplotlib.colors as mcolors
import numpy as np
import time
from collections import Counter
import warnings
from sklearn.exceptions import ConvergenceWarning

'''
Idea of the joint process:
    - We have three bandits: two for advertisement and one for pricing
    - Pricing is a TS bandit
    - Advertisement are GP-UCB and GP-TS bandits
    - Pricing bandit is initialized by pulling every price once
    - Every "PULL_PRICE_EVERY" in TIME_HORIZON step we pull a price from the pricing bandit <=> Could resemble changing the price every 30 days
    - Every time step we pull a bid from each advertisement bandit 
    - At each time step we update the Advertisement with the reward function that takes the bid and the price as input
    - The Pricing Algorithm is trained with conversion Probability as reward
    => This means that the Advertisement bandits depend on the Pricing bandit, whereas the pricing bandit does not
    - Redo this for N_EXPERIMENTS*TIME_HORIZON
'''

#Utilities
colour = colours()
harry_plotter = plot_helper()


#Getting rid of convergence warnings but note we should asjust the GP parameters
warnings.filterwarnings("ignore", category=ConvergenceWarning)

#Hyperparameters for Advertisement
N_ARMS = 100 #We learn advertisement
LEARNING_COST = True
USED_ALGORITHM = "GP-UCB"
Mock_Advertisement_UCB = mock_process_advertisment(N_ARMS, "C1", USED_ALGORITHM, LEARNING_COST)
USED_ALGORITHM = "GP-TS"
Mock_Advertisement_TS = mock_process_advertisment(N_ARMS, "C1", USED_ALGORITHM, LEARNING_COST)
UPDATE_EVERY = 10
#Hyperparameters for the Learning Process
TIME_HORIZON = 365
N_EXPERIMENTS = 5

#Hyperparameters for Pricing
N_ARMS = 5 #We learn pricing
USED_ALGORITHM = "TS"
Mock_Pricing = mock_process(N_ARMS, USED_ALGORITHM, "C1")
PULL_PRICE_EVERY = 2

# Returning the Clairvoyant optimal candidate (tuple of price and bid) that maximizes the reward
Mock_Clairvoyant = clairvoyant_process(5, "C1")
clairvoyant_candidate = Mock_Clairvoyant.get_optimal_candidate("joint")
clairvoyant_bid = clairvoyant_candidate[1]
clairvoyant_price = clairvoyant_candidate[0]
clairvoyant_reward = clairvoyant_candidate[2]
clairvoyant_bid_as_arm = Mock_Advertisement_TS.return_arm_for_bid(clairvoyant_bid)
clairvoyant_price_as_arm = Mock_Pricing.return_arm_for_price(clairvoyant_price)
#Clairvoyant Reward 
Clairvoyant_Rewards = []
#Regret Variables
UCB_Regret = []
TS_Regret = []

start_time = time.time()
for t in range(0, TIME_HORIZON):
    # PULLING PRICE
    if t != 0:
        Mock_Advertisement_UCB.update_model() # real value
        Mock_Advertisement_TS.update_model() # r
    print(colour.cyanify("----------------"))
    print(f"Time {t+1}/ {TIME_HORIZON}")
    if t % PULL_PRICE_EVERY == 0:
        
        
        predicted_price_as_arm = Mock_Pricing.pull_arm() #ARM of PRICE
        print(colour.redify(f"TS-Pricing | pulled arm {predicted_price_as_arm}"))
        predicted_arm_as_price = Mock_Pricing.return_price_for_arm(predicted_price_as_arm) # PRICE
        print(colour.redify(f"TS-Pricing | pulled price {predicted_arm_as_price}"))
    for e in range(0, N_EXPERIMENTS):
        # THOMPSON SAMPLING
        predicted_value_UCB = Mock_Advertisement_UCB.pull_arm() # estimated by MAB => Returns the bid not its index
        predicted_value_TS = Mock_Advertisement_TS.pull_arm() # estimated by MAB => Returns the bid not its index
        predicted_value_as_bid_UCB = Mock_Advertisement_UCB.return_bid_for_arm(predicted_value_UCB)
        predicted_value_as_bid_TS = Mock_Advertisement_TS.return_bid_for_arm(predicted_value_TS)

        # then, get the real (randomized reward for comparison)
        target_value_UCB = Mock_Advertisement_UCB.get_reward("C1", predicted_arm_as_price, predicted_value_as_bid_UCB) # real value
        target_value_TS = Mock_Advertisement_TS.get_reward("C1", predicted_arm_as_price, predicted_value_as_bid_TS) # real value
        UCB_Regret.append(clairvoyant_reward - target_value_UCB)
        TS_Regret.append(clairvoyant_reward - target_value_TS)
        Mock_Pricing.update_parameters(Mock_Pricing.configuration["price_curve"][predicted_arm_as_price], predicted_price_as_arm) # real value
        
    
        Mock_Advertisement_UCB.update_parameters(target_value_UCB, predicted_value_UCB) # real value
        
        Mock_Advertisement_TS.update_parameters( target_value_TS, predicted_value_TS) # real value
        
        if e % UPDATE_EVERY == 0:
             pass
        #CLAIRVOYANT Reward Cumulation
        Clairvoyant_Rewards.append(clairvoyant_reward)
    


print(colour.greenify("UCB pulled arms:"))
print(Counter(Mock_Advertisement_UCB.pulled_arms))
print(colour.greenify("TS pulled arms:"))
print(Counter(Mock_Advertisement_TS.pulled_arms))
print(colour.redify("Accumulated rewards:"))
print(f"UCB: {round(sum(Mock_Advertisement_UCB.collected_rewards), 2)}")
print(f"TS: {round(sum(Mock_Advertisement_TS.collected_rewards), 2)}")
print(f"Time elapsed: {round(time.time() - start_time, 2)} seconds")
average_reward_TS = harry_plotter.calculate_average_reward(Mock_Advertisement_TS.collected_rewards, Mock_Advertisement_TS.pulled_arms)
average_reward_UCB = harry_plotter.calculate_average_reward(Mock_Advertisement_UCB.collected_rewards, Mock_Advertisement_UCB.pulled_arms)
#The following is needed to check whether every arm has been pulled
average_reward_TS = harry_plotter.fill_empty_values(average_reward_TS, Mock_Advertisement_TS.n_arms)
average_reward_UCB = harry_plotter.fill_empty_values(average_reward_UCB, Mock_Advertisement_UCB.n_arms)


plt.figure(0)

plt.ylabel("Cumulative Reward Advertisement")

plt.xlabel("Experiment")

plt.plot(np.cumsum(np.mean(np.array([Mock_Advertisement_TS.collected_rewards]), axis = 0)), 'cyan')

plt.plot(np.cumsum(np.mean(np.array([Mock_Advertisement_UCB.collected_rewards]), axis = 0)), 'green')

plt.plot(np.cumsum(np.mean(np.array([Clairvoyant_Rewards]), axis = 0)), 'blue')

#plt.plot(np.cumsum(np.mean(np.array([CV_rewards_per_experiment]), axis = 0)), 'b')

plt.legend(["GP-TS", "GP-UCB", "Clairvoyant"])

plt.savefig("graphs/step_3/Cumulative_Reward_TS_UCB.png")

plt.show()




#Plot instantaneous reward

plt.figure(1)

plt.ylabel("Instantaneous Reward Advertisement")

plt.xlabel("Experiment")

plt.plot(np.mean(np.array([Mock_Advertisement_TS.collected_rewards]), axis = 0), 'cyan')

plt.plot(np.mean(np.array([Mock_Advertisement_UCB.collected_rewards]), axis = 0), 'green')

plt.plot(np.mean(np.array([Clairvoyant_Rewards]), axis = 0), 'b')


plt.legend(["GP-TS", "GP-UCB", "Clairvoyant"])

plt.savefig("graphs/step_3/Instantaneous_Reward_TS_UCB.png")

plt.show()


#Plotting each reward over time for TS including its respective standard deviation

moved_mean_TS, moved_standard_TS = harry_plotter.calculate_moving_average(Mock_Advertisement_TS.collected_rewards, N_EXPERIMENTS)

x_axis = np.arange(1, len(moved_mean_TS) + 1)

plt.figure(1)

plt.plot(x_axis, moved_mean_TS, label = "GP-TS Average reward", color = "cyan")

plt.errorbar(x_axis, moved_mean_TS, yerr = moved_standard_TS, label = "TS Standard Deviation", linestyle = 'None', marker = 'o', color = "grey")

plt.xlabel("Time Steps")

plt.ylabel("Reward")

plt.title("GP-TS Average Reward incl. Standard Deviation")

plt.legend()

plt.savefig("graphs/step_3/TS_Average_Reward_incl_Standard_Deviation.png")

plt.show()

#Plotting each reward over time for UCB including its respective standard deviation
moved_mean_UCB, moved_standard_UCB = harry_plotter.calculate_moving_average(Mock_Advertisement_UCB.collected_rewards, N_EXPERIMENTS)
# Generating values along the x axis
x_axis = np.arange(1, len(moved_mean_UCB) + 1)

plt.figure(2)

plt.plot(x_axis, moved_mean_UCB, label = "GP-UCB Average reward", color = "green")

plt.errorbar(x_axis, moved_mean_UCB, yerr = moved_standard_UCB, label = "GP-UCB Standard Deviation", linestyle = 'None', marker = 'o', color = "grey")

plt.xlabel("Time Steps")

plt.ylabel("Reward")

plt.title("GP-UCB Average Reward incl. Standard Deviation")

plt.legend()

plt.savefig("graphs/step_3/UCB_Average_Reward_incl_Standard_Deviation.png")

plt.show()


reward_per_time_step_TS = harry_plotter.calculate_experiment_sums(Mock_Advertisement_TS.collected_rewards, N_EXPERIMENTS)
reward_per_time_step_UCB = harry_plotter.calculate_experiment_sums(Mock_Advertisement_UCB.collected_rewards, N_EXPERIMENTS)
reward_per_time_step_CV = harry_plotter.calculate_experiment_sums(Clairvoyant_Rewards, N_EXPERIMENTS)

#Plot instantaneous reward

plt.figure(3)

plt.ylabel("Instantaneous Reward per Time Step")

plt.xlabel("Time")

plt.plot(reward_per_time_step_TS, 'cyan')

plt.plot(reward_per_time_step_UCB, 'green')

plt.plot(reward_per_time_step_CV, 'blue')

""" plt.plot(np.mean(np.array([UCB_objective_rewards_per_experiment]), axis = 0), 'g')

plt.plot(np.mean(np.array([CV_rewards_per_experiment]), axis = 0), 'b') """

plt.legend(["GP Thompson Sampling", "GP UCB", "Clairvoyant"])

plt.savefig("graphs/step_3/Instantaneous_Reward_per_Time_Step_TS_UCB.png")

plt.show()


cumulative_reward_per_time_step_TS = harry_plotter.calculate_accumulated_experiment_sums(Mock_Advertisement_TS.collected_rewards, N_EXPERIMENTS)
cumulative_reward_per_time_step_UCB = harry_plotter.calculate_accumulated_experiment_sums(Mock_Advertisement_UCB.collected_rewards, N_EXPERIMENTS)
cumulative_reward_per_time_step_CV = harry_plotter.calculate_accumulated_experiment_sums(Clairvoyant_Rewards, N_EXPERIMENTS)

#Plot instantaneous reward

plt.figure(4)

plt.ylabel("Cumulative Reward per Time Step")

plt.xlabel("Time")

plt.plot(cumulative_reward_per_time_step_TS, 'cyan')

plt.plot(cumulative_reward_per_time_step_UCB, 'green')

plt.plot(cumulative_reward_per_time_step_CV, 'blue')

""" plt.plot(np.mean(np.array([UCB_objective_rewards_per_experiment]), axis = 0), 'g')

plt.plot(np.mean(np.array([CV_rewards_per_experiment]), axis = 0), 'b') """

plt.legend(["GP Thompson Sampling", "GP UCB", "Clairvoyant"])

plt.savefig("graphs/step_3/Cumulative_Reward_per_Time_Step_TS_UCB.png")

plt.show()

#Plotting the cost/click curve based on rewards retrieved by the algorithms for every bid

if LEARNING_COST:
    LEARNING_TARGET = "cost"
else:
    LEARNING_TARGET = "clicks"

pull_counts_normalized_TS = harry_plotter.normalize_arm_counts(Mock_Advertisement_TS.arm_counter)
cmap = mcolors.LinearSegmentedColormap.from_list("mycmap", ["red", "green"])


plt.figure(2)

plt.ylabel(f"{LEARNING_TARGET} | Average Reward per arm")
plt.xlabel("Arm")

# Scatter plot
scatter = plt.scatter(average_reward_TS.keys(), average_reward_TS.values(), c=pull_counts_normalized_TS, cmap=cmap)

plt.colorbar(label='Number of times pulled')
plt.legend(["GP-TS"])

# Adjust x-axis ticks and labels
x_values = average_reward_TS.keys()

plt.savefig("graphs/step_3/TS_Average_Reward_per_Arm.png")

plt.show()

pull_counts_normalized_UCB = harry_plotter.normalize_arm_counts(Mock_Advertisement_UCB.arm_counter)


plt.figure(3)

plt.ylabel(f"{LEARNING_TARGET} Average Reward per arm")

plt.xlabel("Arm")

plt.scatter(average_reward_UCB.keys(), average_reward_UCB.values(), c=pull_counts_normalized_UCB, cmap=cmap)

plt.colorbar(label='Number of times pulled')

plt.legend(["GP-UCB"])

plt.savefig("graphs/step_3/UCB_Average_Reward_per_Arm.png")

plt.show()

#Plot instantaneous regret

regret_per_time_step_TS = harry_plotter.calculate_experiment_sums(TS_Regret, N_EXPERIMENTS)
regret_per_time_step_UCB = harry_plotter.calculate_experiment_sums(UCB_Regret, N_EXPERIMENTS)

#Plot instantaneous regret

plt.figure(5)

plt.ylabel("Instantaneous Regret per Time Step")

plt.xlabel("Time")

plt.plot(regret_per_time_step_TS, 'cyan')

plt.plot(regret_per_time_step_UCB, 'green')

plt.legend(["Thompson Sampling", "UCB1"])

plt.savefig("graphs/step_3/Instantaneous_Regret_per_Time_Step_TS_UCB.png")

plt.show()

cumulative_regret_per_time_step_TS = harry_plotter.calculate_accumulated_experiment_sums(TS_Regret, N_EXPERIMENTS)
cumulative_regret_per_time_step_UCB = harry_plotter.calculate_accumulated_experiment_sums(UCB_Regret, N_EXPERIMENTS)

#Plot cumulative regret

plt.figure(6)

plt.ylabel("Cumulative Regret per Time Step")

plt.xlabel("Time")

plt.plot(cumulative_regret_per_time_step_TS, 'cyan')

plt.plot(cumulative_regret_per_time_step_UCB, 'green')

plt.legend(["Thompson Sampling", "UCB1"])

plt.savefig("graphs/step_3/Cumulative_Regret_per_Time_Step_TS_UCB.png")

plt.show()
