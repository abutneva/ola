import numpy as np
from abc import ABC, abstractmethod
from abstract import mock_process_advertisment
from util import feature_helper, context_helper
'''
The following class defines a abstract sceleton/parent class for the context generation of the bandit problem.

'''
class AbstractContextGenerator(ABC):
    def __init__(self):
        pass
    
    @abstractmethod
    def generate_context(self, data):
        pass

    @abstractmethod
    def update(self, observation):
        pass

from sklearn.tree import DecisionTreeClassifier
import numpy as np

class DecisionTreeContextGenerator:
    def __init__(self, n_of_features=2):
        self.model = DecisionTreeClassifier(random_state=0)
        self.features_rewards = []
        self.split_created = False

    def initialize(self):
        self.features_rewards.append((0, 0, 0))
        self.model.fit(self.features_rewards, [0])  # fit with initial data

    def generate_context(self):
        # Only proceed if enough data has been collected
        if len(self.features_rewards) > 1:
            # predict the contexts for all observations
            predicted_contexts = self.model.predict(self.features_rewards)
            print(f"Predicted contexts: {predicted_contexts}")
            # Calculate the mean rewards for each predicted context
            mean_rewards = {}
            for context in set(predicted_contexts):
                print(f"Context: {context}")
                context_rewards = [obs[2] for obs, predicted in zip(self.features_rewards, predicted_contexts) if predicted == context]
                mean_rewards[context] = np.mean(context_rewards)
                print(f"Mean reward: {mean_rewards[context]}")

            # Determine whether splitting the data into contexts improves the overall reward
            overall_mean_reward = np.mean([obs[2] for obs in self.features_rewards])
            max_context_reward = max(mean_rewards.values())
            print(f"Overall mean reward: {overall_mean_reward}")
            print(f"Max context reward: {max_context_reward}")

            if max_context_reward > overall_mean_reward:
                print("Creating new split")
                self.split_created = True

                # Retrain the decision tree model on the new split
                self.model.fit(self.features_rewards, predicted_contexts)
            else:
                print("No new split")

    def update(self, observation):
        print(f"Observation: {observation}")
        self.features_rewards.append(observation)

    def get_number_of_contexts(self):
        if self.split_created:
            # If a split has been created, predict the contexts for all observations
            predicted_contexts = self.model.predict(self.features_rewards)
            return len(set(predicted_contexts))
        else:
            return 1

class SplitConditionContextGenerator(AbstractContextGenerator):
    def __init__(self, n_of_features=2):
        self.n_of_features = n_of_features
        self.features_rewards = [] #should be a list of a tuple of features and reward [((feature1, feature2), reward), ...]
        self.feature_comb_dict = {"1": (0, 0), "2": (0, 1), "3": (1, 0), "4": (1, 1)}
        self.feature_total_reward_of_comb = {"1": 0, "2": 0, "3": 0, "4": 0}
        self.feature_mean_reward_of_comb = {"1": 0, "2": 0, "3": 0, "4": 0}
        self.feature_helper = feature_helper()
        self.feature_comb_count_dict = {"1": 0, "2": 0, "3": 0, "4": 0}
        self.total_mean_reward = 0
        self.total_reward = 0
        self.total_count = 0
        self.Hoeffding_bound = 0
        self.no_of_splits = 0
        self.split_recorder = {}
    def update(self, observation):
        #Overall
        self.total_reward += observation[1]
        self.total_count += 1
        self.total_mean_reward = self.total_reward / self.total_count
        self.Hoeffding_bound = np.sqrt((np.log(1/0.05))/(2*self.total_count))
        #For the specific pulled combination
        comb = self.feature_helper.get_number_for_feature_set(observation[0])
        self.feature_total_reward_of_comb[comb] += observation[1]
        self.feature_comb_count_dict[comb] += 1
        self.feature_mean_reward_of_comb[comb] = self.feature_total_reward_of_comb[comb] / self.feature_comb_count_dict[comb]
        self.split_first_branch = False
        self.split_second_branch = False
        self.split_on_f1 = False
        self.split_on_f2 = False
        


    def have_enough_data(self):

        for val in self.feature_comb_count_dict.values():
            if val == 0:
                return False
    def generate_context(self):
        #Test_1
        LCB = self.total_mean_reward + self.Hoeffding_bound
        if self.have_enough_data:
            #print(self.total_count)
            prob_dict = {}
            for key in self.feature_comb_count_dict:
                prob_dict[key] = self.feature_comb_count_dict[key] / self.total_count
            mean_dict = {}
            for key in self.feature_mean_reward_of_comb:
                if self.feature_comb_count_dict[key] == 0:
                    mean_dict[key] = 0
                else:
                    mean_dict[key] = self.feature_total_reward_of_comb[key] / self.feature_comb_count_dict[key]
            #Testing first split decision (0,1) and (0,0)
            if self.no_of_splits == 0 and self.split_on_f1 == False:

                Number_of_Test_Value_1 = self.feature_helper.get_number_for_feature_set((0,1))
                Number_of_Test_Value_2 = self.feature_helper.get_number_for_feature_set((0,0))
                Test_Value = prob_dict[Number_of_Test_Value_1]*mean_dict[Number_of_Test_Value_1] + prob_dict[Number_of_Test_Value_2]*mean_dict[Number_of_Test_Value_2]
                if Test_Value > LCB:
                    self.no_of_splits += 1
                    print("Splitting on 0,1 and 0,0")
                    new_structure = {"C1": [(0,0), (1,0)], "C2": [(0,1), (1,1)]}
                    print(f"NEW STRUCTURE {new_structure}")
                    self.split_recorder[self.total_count] = f"Split on 0,1 and 0,0 at {self.total_count} => Split Along F2"
                    self.split_on_f2 = True
                    return {"C1": [(0,0), (1,0)], "C2": [(0,1), (1,1)]}

            elif self.no_of_splits == 1 and self.split_first_branch == False:
                Number_of_Test_Value_1 = self.feature_helper.get_number_for_feature_set((0,1))
                Number_of_Test_Value_2 = self.feature_helper.get_number_for_feature_set((1,1))
                Test_Value = prob_dict[Number_of_Test_Value_1]*mean_dict[Number_of_Test_Value_1] + prob_dict[Number_of_Test_Value_2]*mean_dict[Number_of_Test_Value_2]
                if Test_Value > LCB:
                    self.no_of_splits += 1
                    self.split_recorder[self.total_count] = f"Split on 0,1 and 1,1 at {self.total_count} => Split Along F1 (First Branch) (Wrong Split)"
                    self.split_first_branch = True
                    new_structure ={"C1": [(0,1), (1,1)], "C2": (0,0), "C3": (1,0)}
                    print(f"NEW STRUCTURE {new_structure}")
                    return {"C1": [(0,1), (1,1)], "C2": (0,0), "C3": (1,0)}
                Number_of_Test_Value_1 = self.feature_helper.get_number_for_feature_set((0,0))
                Number_of_Test_Value_2 = self.feature_helper.get_number_for_feature_set((1,0))
                Test_Value = prob_dict[Number_of_Test_Value_1]*mean_dict[Number_of_Test_Value_1] + prob_dict[Number_of_Test_Value_2]*mean_dict[Number_of_Test_Value_2]
                
                if Test_Value > LCB:
                    self.no_of_splits += 1
                    self.split_recorder[self.total_count+1] = f"Split on 0,0 and 1,0 at {self.total_count}"
                    self.split_second_branch = True
                    print("Splitting on 0,0 and 1,0 => Correct Final Split")
                    new_structure ={"C1": [(0,0), (1,0)], "C2": (0,1), "C3": (1,1)} 
                    print(f"NEW STRUCTURE {new_structure}")
                    return {"C1": [(0,0), (1,0)], "C2": (0,1), "C3": (1,1)} #Right One

            elif self.no_of_splits == 0 and self.split_on_f2 == False:

                Number_of_Test_Value_1 = self.feature_helper.get_number_for_feature_set((1,0))
                Number_of_Test_Value_2 = self.feature_helper.get_number_for_feature_set((0,0))
                Test_Value = prob_dict[Number_of_Test_Value_1]*mean_dict[Number_of_Test_Value_1] + prob_dict[Number_of_Test_Value_2]*mean_dict[Number_of_Test_Value_2]
                if Test_Value > LCB:
                    self.no_of_splits += 1
                    self.split_recorder[self.total_count+1] = f"Split on 1,0 and 0,0 at {self.total_count}"
                    print("Splitting on 1,0 and 0,0")
                    self.split_on_f1 = True

            elif self.no_of_splits == 1 and self.split_first_branch == False:
                Number_of_Test_Value_1 = self.feature_helper.get_number_for_feature_set((1,0))
                Number_of_Test_Value_2 = self.feature_helper.get_number_for_feature_set((1,1))
                Test_Value = prob_dict[Number_of_Test_Value_1]*mean_dict[Number_of_Test_Value_1] + prob_dict[Number_of_Test_Value_2]*mean_dict[Number_of_Test_Value_2]
                if Test_Value > LCB:
                    self.no_of_splits += 1
                    self.split_first_branch = True
                    self.split_recorder[self.total_count] = f"Split on 1,0 and 1,1 at {self.total_count}"
                Number_of_Test_Value_1 = self.feature_helper.get_number_for_feature_set((0,0))
                Number_of_Test_Value_2 = self.feature_helper.get_number_for_feature_set((0,1))
                Test_Value = prob_dict[Number_of_Test_Value_1]*mean_dict[Number_of_Test_Value_1] + prob_dict[Number_of_Test_Value_2]*mean_dict[Number_of_Test_Value_2]
                if Test_Value > LCB:
                    self.no_of_splits += 1
                    self.split_recorder[self.total_count+1] = f"Split on 0,0 and 0,1 at {self.total_count}"
                    self.split_second_branch = True
                    print("Splitting on 0,0 and 0,1")

    def get_no_of_splits(self):
        return self.no_of_splits
    
    def get_detailed_split_recording(self):
        for rec in self.split_recorder.values():
            print(rec)
            
        #(0,0) and (0,1), (0,1) and (1,1),  (1,0) and (0,0),  (1,0) and (1,1)
        
        #Testing second split decision (1,0) and (1,0)


class context_bandit_wrapper():

    def __init__(self,  used_algorithm:str, feature_comb_dict= {"C1": (0,0), "C2": (0,1), "C3": [(1,0), (1,1)]}):
        '''
        If feature comb dict is not provided, it will use the default one corresponding to the known structure case
        '''
        self.used_algorithm = used_algorithm
        self.feature_comb_dict = feature_comb_dict
        self.bandit_dict = self.initialize_bandits(used_algorithm=used_algorithm)

    def initialize_bandits(self,used_algorithm: str, n_arms: int =100, user_class: str="C1",  learning_costs:bool = True):
        bandit_dict = {}
        for i in self.feature_comb_dict.keys():
            bandit_dict[i] = mock_process_advertisment(n_arms, user_class, used_algorithm, learning_costs)
        return bandit_dict
    
    def reinitialize_bandits(self, used_algorithm: str, n_arms: int =100, user_class: str="C1",  learning_costs:bool = True):
        self.bandit_dict = self.initialize_bandits(used_algorithm=used_algorithm, n_arms=n_arms, user_class=user_class, learning_costs=learning_costs)
    
    def get_context_for_feature_combination(self, feature_set):
            for key, value in self.feature_comb_dict.items():
                if type(value) == list:
                    for val in value:
                        if val == feature_set:
                            return key
                if value == feature_set:
                    return key
            
    def get_feature_combination_for_context(self, context):
          for key, value in self.feature_comb_dict.items():
            if key == context:
                return value

    def change_structure(self, feature_comb_dict):
        self.feature_comb_dict = feature_comb_dict
        self.reinitialize_bandits(self.used_algorithm)

    def pull_arm(self, feature_combination):
        associated_context = self.get_context_for_feature_combination(feature_combination)
        return self.bandit_dict[associated_context].pull_arm()
    
    def update(self, feature_combination, reward, predicted_value):
        associated_context = self.get_context_for_feature_combination(feature_combination)
        self.bandit_dict[associated_context].update(predicted_value, reward)

    def update_parameters(self, feature_combination, reward, predicted_value):
        associated_context = self.get_context_for_feature_combination(feature_combination)
        self.bandit_dict[associated_context].update_parameters(reward, predicted_value)

    def update_model(self, feature_combination):
        associated_context = self.get_context_for_feature_combination(feature_combination)
        self.bandit_dict[associated_context].update_model()