from abstract import mock_process, clairvoyant_process
from util import colours, plot_helper
import matplotlib.pyplot as plt
import numpy as np
#Utilities
colour = colours()
harry_plotter = plot_helper()
from collections import Counter

#Hyperparameters
N_ARMS = 5 #We learn pricing
USED_ALGORITHM = "UCB"
Mock_UCB = mock_process(N_ARMS, USED_ALGORITHM, "C1", None)
TIME_HORIZON = 365
N_EXPERIMENTS = 100
SEASONS_CHANGE_EVERY = 70
SEASONS = ["beachbody", "new year resolution", "lazy", "4th season", "5th season"]
#UCB
UCB_rewards_per_experiment = []
UCB_cumulative_rewards_per_experiment = []
UCB_objective_rewards_per_experiment = []
UCB_objective_cumulative_rewards_per_experiment = []
UCB_objective_regret_per_experiment = []

USED_ALGORITHM = "SW-UCB"
WINDOW_SIZE = 6000
Mock_UCB_SW = mock_process(N_ARMS, USED_ALGORITHM, "C1", window_size=WINDOW_SIZE)
#SW_UCB
SW_UCB_rewards_per_experiment = []
SW_UCB_cumulative_rewards_per_experiment = []
SW_UCB_objective_rewards_per_experiment = []
SW_UCB_objective_cumulative_rewards_per_experiment = []
SW_UCB_objective_regret_per_experiment = []
cum_rew = 0

USED_ALGORITHM = "CD-UCB"
Mock_UCB_CD = mock_process(N_ARMS, USED_ALGORITHM, "C1")
#CD_UCB
СD_UCB_rewards_per_experiment = []
СD_UCB_rewards_per_experiment
CD_UCB_cumulative_rewards_per_experiment = []
CD_UCB_objective_rewards_per_experiment = []
CD_UCB_objective_cumulative_rewards_per_experiment = []
CD_UCB_objective_regret_per_experiment = []
TAU=2

USED_ALGORITHM = "EXP3"
GAMMA = 0.4
Mock_EXP3 = mock_process(N_ARMS, USED_ALGORITHM, "C1", gamma=GAMMA)
#EXP3
EXP3_rewards_per_experiment = []
EXP3_cumulative_rewards_per_experiment = []
EXP3_objective_rewards_per_experiment = []
EXP3_objective_cumulative_rewards_per_experiment = []
EXP3_objective_regret_per_experiment = []


# Clairvoyant
CV_rewards_per_experiment = []
CV_cumulative_rewards_per_experiment = []
Mock_CV = clairvoyant_process(N_ARMS, "C1")

Mock_UCB.initialize()
Mock_UCB_SW.initialize()
Mock_UCB_CD.initialize()
counter = 0
for t in range(0, TIME_HORIZON):
    
    if t % SEASONS_CHANGE_EVERY == 0:
        SEASON = SEASONS[counter % len(SEASONS)]
        print(colour.greenify(f"Current Season is {SEASON}"))
        counter += 1
        Mock_UCB.change_configuration(SEASON)
        Mock_UCB_SW.change_configuration(SEASON)
        Mock_UCB_CD.change_configuration(SEASON)
        Mock_CV.change_configuration(SEASON)
        Mock_EXP3.change_configuration(SEASON)
        clairvoyant_arm_and_reward = Mock_CV.pull_best_arm()
        clairvoyant_arm = clairvoyant_arm_and_reward[0]
        clairvoyant_reward = clairvoyant_arm_and_reward[1]

        print(colour.greenify(f"Clairvoyant arm is {clairvoyant_arm}"))
        print(colour.greenify(f"Clairvoyant reward is {clairvoyant_reward}"))
    print(f"Time step: {t}/{TIME_HORIZON}")
    for e in range(0, N_EXPERIMENTS):

        # UCB
         # first, predict the reward with MAB
        predicted_value_UCB = Mock_UCB.pull_arm() # estimated by MAB
        predicted_value_as_price_UCB = Mock_UCB.return_price_for_arm(predicted_value_UCB)
        # then, get the real (randomized reward for comparison)
        target_value_UCB = Mock_UCB.get_target_value(predicted_value_as_price_UCB) # real value

        # UCB_SW
         # first, predict the reward with MAB
        predicted_value_SW_UCB = Mock_UCB_SW.pull_arm() # estimated by MAB
        predicted_value_as_price_SW_UCB = Mock_UCB_SW.return_price_for_arm(predicted_value_SW_UCB)
        # then, get the real (randomized reward for comparison)
        target_value_SW_UCB = Mock_UCB_SW.get_target_value(predicted_value_as_price_SW_UCB) # real value

        # UCB_CD
         # first, predict the reward with MAB
        predicted_value_CD_UCB = Mock_UCB_CD.pull_arm() # estimated by MAB
        predicted_value_as_price_CD_UCB = Mock_UCB_CD.return_price_for_arm(predicted_value_CD_UCB)
        # then, get the real (randomized reward for comparison)
        target_value_CD_UCB = Mock_UCB_CD.get_target_value(predicted_value_as_price_CD_UCB) # real value

        # EXP3
        predicted_value_EXP3 = Mock_EXP3.pull_arm() # estimated by MAB
        predicted_value_as_price_EXP3 = Mock_EXP3.return_price_for_arm(predicted_value_EXP3)
        # then, get the real (randomized reward for comparison)
        target_value_EXP3 = Mock_EXP3.get_target_value(predicted_value_as_price_EXP3) # real value


        # CLAIRVOYANT
        Objective_UCB_Reward = Mock_CV.find_best_bid(predicted_value_as_price_UCB)[1]
        Objective_UCB_SW_Reward = Mock_CV.find_best_bid(predicted_value_as_price_SW_UCB)[1]
        Objective_UCB_CD_Reward = Mock_CV.find_best_bid(predicted_value_as_price_CD_UCB)[1]
        Objective_EXP3_Reward = Mock_CV.find_best_bid(predicted_value_as_price_EXP3)[1]

        #Calculating Regret
        Objective_UCB_Regret = clairvoyant_reward - Objective_UCB_Reward
        Objective_UCB_SW_Regret = clairvoyant_reward - Objective_UCB_SW_Reward
        Objective_UCB_CD_Regret = clairvoyant_reward - Objective_UCB_CD_Reward
        Objective_EXP3_Regret = clairvoyant_reward - Objective_EXP3_Reward
        # update parameters for UCB
        parameters_UCB = Mock_UCB.update_parameters(target_value_UCB, predicted_value_UCB, None)

        # update parameters for UCB_SW
        parameters_UCB_SW = Mock_UCB_SW.update_parameters(target_value_SW_UCB, predicted_value_SW_UCB, cum_rew)

        # update parameters for UCB_CD
        parameters_UCB_CD = Mock_UCB_CD.update_parameters(target_value_CD_UCB, predicted_value_CD_UCB, tau=TAU)

        # update parameters for EXP3
        parameters_EXP3 = Mock_EXP3.update_parameters(target_value_EXP3, predicted_value_EXP3)


        # Append the rewards to the list for plotting purposes
        # Clairvoyant
        CV_rewards_per_experiment.append(clairvoyant_reward)
        
        #UCB
        UCB_rewards_per_experiment.append(target_value_UCB)
        UCB_cumulative_rewards_per_experiment.append(sum(UCB_rewards_per_experiment))
        UCB_objective_rewards_per_experiment.append(Objective_UCB_Reward)
        UCB_objective_cumulative_rewards_per_experiment.append(sum(UCB_objective_rewards_per_experiment))
        UCB_objective_regret_per_experiment.append(Objective_UCB_Regret)


        #SW-UCB
        SW_UCB_rewards_per_experiment.append(target_value_SW_UCB)
        SW_UCB_cumulative_rewards_per_experiment.append(sum(SW_UCB_rewards_per_experiment))
        SW_UCB_objective_rewards_per_experiment.append(Objective_UCB_SW_Reward)
        SW_UCB_objective_cumulative_rewards_per_experiment.append(sum(SW_UCB_objective_rewards_per_experiment))
        SW_UCB_objective_regret_per_experiment.append(Objective_UCB_SW_Regret)

        #CD-UCB
        СD_UCB_rewards_per_experiment.append(target_value_CD_UCB)
        CD_UCB_cumulative_rewards_per_experiment.append(sum(СD_UCB_rewards_per_experiment))
        CD_UCB_objective_rewards_per_experiment.append(Objective_UCB_CD_Reward)
        CD_UCB_objective_cumulative_rewards_per_experiment.append(sum(CD_UCB_objective_rewards_per_experiment))
        CD_UCB_objective_regret_per_experiment.append(Objective_UCB_CD_Regret)

        #EXP3
        EXP3_rewards_per_experiment.append(target_value_EXP3)
        EXP3_cumulative_rewards_per_experiment.append(sum(EXP3_rewards_per_experiment))
        EXP3_objective_rewards_per_experiment.append(Objective_EXP3_Reward)
        EXP3_objective_cumulative_rewards_per_experiment.append(sum(EXP3_objective_rewards_per_experiment))
        EXP3_objective_regret_per_experiment.append(Objective_EXP3_Regret)

    '''
    for i in range(N_PHASES):
        t_index = range(i*PHASES_LEN, (i+1)*PHASES_LEN)
        opt_per_round[t_index] = opt_per_phase[i]
        UCB_instaneous_regret[t_index] = opt_per_phase[i] - np.mean(UCB_objective_rewards_per_experiment, axis = 0)[t_index]
        SW_UCB_instaneous_regret[t_index] = opt_per_phase[i] - np.mean(SW_UCB_objective_rewards_per_experiment, axis = 0)[t_index]  
    '''  


# Count the number of times each arm was pulled within the pulled_arms_list

print(colour.greenify("UCB rewards:"))
print(sum(UCB_objective_rewards_per_experiment))

print(colour.greenify("SW-UCB rewards:"))
print(sum(SW_UCB_objective_rewards_per_experiment))

print(colour.greenify("CD-UCB rewards:"))
print(sum(CD_UCB_objective_rewards_per_experiment))

print(colour.greenify("EXP3 rewards:"))
print(sum(EXP3_objective_rewards_per_experiment))

print(colour.greenify("CV rewards:"))
print(sum(CV_rewards_per_experiment))




#Plot cumulative reward

plt.figure(0)

plt.ylabel("Cumulative Reward")

plt.xlabel("Time")

plt.plot(np.cumsum(np.mean(np.array([UCB_objective_rewards_per_experiment]), axis = 0)), 'green')
plt.plot(np.cumsum(np.mean(np.array([SW_UCB_objective_rewards_per_experiment]), axis = 0)), 'yellow')
plt.plot(np.cumsum(np.mean(np.array([CD_UCB_objective_rewards_per_experiment]), axis = 0)), 'magenta')
plt.plot(np.cumsum(np.mean(np.array([EXP3_objective_rewards_per_experiment]), axis = 0)), 'orange')

plt.plot(np.cumsum(np.mean(np.array([CV_rewards_per_experiment]), axis = 0)), 'blue')

plt.legend(["UCB1", "SW-UCB", "CD-UCB", "EXP3", "Clairvoyant"])

plt.savefig("graphs/step_6/cumulative_reward.png")

plt.show()


#Calculating the mean at each step to be plotted

#For UCB
# Calculate the cumulative sum of the array
cumulative_sum_UCB = np.cumsum(UCB_objective_rewards_per_experiment)
# Calculate the mean at each index
mean_values_UCB = cumulative_sum_UCB / np.arange(1, len(cumulative_sum_UCB) + 1)
# Calculate the standard deviation at each index
std_values_UCB = np.std(cumulative_sum_UCB[:len(mean_values_UCB)])

#For UCB_SW
# Calculate the cumulative sum of the array
cumulative_sum_UCB_SW = np.cumsum(SW_UCB_objective_rewards_per_experiment)
# Calculate the mean at each index
mean_values_UCB_SW = cumulative_sum_UCB_SW / np.arange(1, len(cumulative_sum_UCB_SW) + 1)
# Calculate the standard deviation at each index
std_values_UCB_SW = np.std(cumulative_sum_UCB_SW[:len(mean_values_UCB_SW)])

#For UCB_CD
# Calculate the cumulative sum of the array
cumulative_sum_UCB_CD = np.cumsum(CD_UCB_objective_rewards_per_experiment)
# Calculate the mean at each index
mean_values_UCB_CD = cumulative_sum_UCB_CD / np.arange(1, len(cumulative_sum_UCB_CD) + 1)
# Calculate the standard deviation at each index
std_values_UCB_CD = np.std(cumulative_sum_UCB_CD[:len(mean_values_UCB_CD)])

#For EXP3
# Calculate the cumulative sum of the array
cumulative_sum_EXP3 = np.cumsum(EXP3_objective_rewards_per_experiment)
# Calculate the mean at each index
mean_values_EXP3 = cumulative_sum_EXP3 / np.arange(1, len(cumulative_sum_EXP3) + 1)
# Calculate the standard deviation at each index
std_values_EXP3 = np.std(cumulative_sum_EXP3[:len(mean_values_EXP3)])

#For Clairvoyant
#For CV
# Calculate the cumulative sum of the array
cumulative_sum_CV = np.cumsum(CV_rewards_per_experiment)
# Calculate the mean at each index
mean_values_CV = cumulative_sum_CV / np.arange(1, len(cumulative_sum_CV) + 1)
# Calculate the standard deviation at each index
std_values_CV = np.std(cumulative_sum_CV[:len(mean_values_CV)])

#Plot Average reward over time
plt.figure("Average Reward over experiments")

plt.ylabel("Average Reward")

plt.xlabel("Experiments")

plt.plot(mean_values_UCB_CD, 'magenta')

plt.plot(mean_values_UCB_SW, 'yellow')

plt.plot(mean_values_UCB, 'green')

plt.plot(mean_values_CV, 'blue')

plt.plot(mean_values_EXP3, 'orange')

plt.legend(["UCB-CD","UCB-SW", "UCB1", "Clairvoyant", "EXP3"])

plt.savefig("graphs/step_6/average_reward.png")

plt.show()
#Plot instantaneous reward

#Moving Mean and Standard Deviation for UCB
moved_mean_UCB, moved_standard_UCB = harry_plotter.calculate_moving_average(UCB_objective_rewards_per_experiment, N_EXPERIMENTS)
# Generating values along the x axis
x_axis = np.arange(1, len(moved_mean_UCB) + 1)

plt.figure("UCB Average Reward incl. Standard Deviation")

plt.plot(x_axis, moved_mean_UCB, label = "UCB Average reward", color="green")

plt.errorbar(x_axis, moved_mean_UCB, yerr = moved_standard_UCB, label = "UCB Standard Deviation", linestyle = 'None', marker = 'o', color = "grey")

plt.xlabel("Time Steps")

plt.ylabel("Reward")

plt.title("UCB Average Reward incl. Standard Deviation")

plt.legend()

plt.savefig("graphs/step_6/UCB_average_reward_incl_standard_dev.png")

plt.show()

#Moving Mean and Standard Deviation for UCB
moved_mean_EXP3, moved_standard_EXP3 = harry_plotter.calculate_moving_average(EXP3_objective_rewards_per_experiment, N_EXPERIMENTS)
# Generating values along the x axis
x_axis = np.arange(1, len(moved_mean_EXP3) + 1)

plt.figure("EXP3 Average Reward incl. Standard Deviation")

plt.plot(x_axis, moved_mean_EXP3, label = "EXP3 Average reward", color = "orange")

plt.errorbar(x_axis, moved_mean_EXP3, yerr = moved_mean_EXP3, label = "EXP3 Standard Deviation", linestyle = 'None', marker = 'o', color = "grey")

plt.xlabel("Time Steps")

plt.ylabel("Reward")

plt.title("EXP3 Average Reward incl. Standard Deviation")

plt.legend()

plt.savefig("graphs/step_6/EXP3_average_reward_incl_standard_dev.png")

plt.show()

#Plotting each reward over time for UCB-SW including its respective standard deviation
moved_mean_UCB_SW, moved_standard_UCB_SW = harry_plotter.calculate_moving_average(SW_UCB_objective_rewards_per_experiment, N_EXPERIMENTS)
# Generating values along the x axis
x_axis = np.arange(1, len(moved_mean_UCB_SW) + 1)

plt.figure("UCB-SW Average Reward incl. Standard Deviation")

plt.plot(x_axis, moved_mean_UCB_SW, label = "UCB-SW Average reward", color = "yellow")

plt.errorbar(x_axis, moved_mean_UCB_SW, yerr = moved_standard_UCB_SW, label = "UCB-SW Standard Deviation", linestyle = 'None', marker = 'o', color = "grey")

plt.xlabel("Time Steps")

plt.ylabel("Reward")

plt.title("UCB-SW Average Reward incl. Standard Deviation")

plt.legend()

plt.savefig("graphs/step_6/UCB_SW_average_reward_incl_standard_dev.png")

plt.show()

#Plotting each reward over time for UCB-SW including its respective standard deviation
moved_mean_UCB_CD, moved_standard_UCB_CD = harry_plotter.calculate_moving_average(CD_UCB_objective_rewards_per_experiment, N_EXPERIMENTS)
# Generating values along the x axis
x_axis = np.arange(1, len(moved_mean_UCB_CD) + 1)

plt.figure("UCB-CD Average Reward incl. Standard Deviation")

plt.plot(x_axis, moved_mean_UCB_CD, label = "UCB-CD Average reward", color = "magenta")

plt.errorbar(x_axis, moved_mean_UCB_CD, yerr = moved_standard_UCB_CD, label = "UCB-SW Standard Deviation", linestyle = 'None', marker = 'o', color = "grey")

plt.xlabel("Time Steps")

plt.ylabel("Reward")

plt.title("UCB-CD Average Reward incl. Standard Deviation")

plt.legend()

plt.savefig("graphs/step_6/UCB_CD_average_reward_incl_standard_dev.png")

plt.show()

reward_per_time_step_UCB_SW = harry_plotter.calculate_experiment_sums(SW_UCB_objective_rewards_per_experiment, N_EXPERIMENTS)
reward_per_time_step_UCB_CD = harry_plotter.calculate_experiment_sums(CD_UCB_objective_rewards_per_experiment, N_EXPERIMENTS)
reward_per_time_step_UCB = harry_plotter.calculate_experiment_sums(UCB_objective_rewards_per_experiment, N_EXPERIMENTS)
reward_per_time_step_EXP3 = harry_plotter.calculate_experiment_sums(EXP3_objective_rewards_per_experiment, N_EXPERIMENTS)
reward_per_time_step_CV = harry_plotter.calculate_experiment_sums(CV_rewards_per_experiment, N_EXPERIMENTS)

#Plot instantaneous reward

plt.figure(3)

plt.ylabel("Instantaneous Reward per Time Step")

plt.xlabel("Time")

plt.plot(reward_per_time_step_UCB_SW, 'yellow')

plt.plot(reward_per_time_step_UCB_CD, 'magenta')

plt.plot(reward_per_time_step_UCB, 'green')

plt.plot(reward_per_time_step_EXP3, 'orange')

plt.plot(reward_per_time_step_CV, 'blue')

plt.legend(["UCB-SW","UCB-CD" ,"UCB1", "EXP3" , "Clairvoyant"])

plt.savefig("graphs/step_6/instantaneous_reward_per_time_step.png")

plt.show()


regret_per_time_step_UCB_SW = harry_plotter.calculate_experiment_sums(SW_UCB_objective_regret_per_experiment, N_EXPERIMENTS)
regret_per_time_step_UCB_CD = harry_plotter.calculate_experiment_sums(CD_UCB_objective_regret_per_experiment, N_EXPERIMENTS)
regret_per_time_step_UCB = harry_plotter.calculate_experiment_sums(UCB_objective_regret_per_experiment, N_EXPERIMENTS)
regret_per_time_step_EXP3 = harry_plotter.calculate_experiment_sums(EXP3_objective_regret_per_experiment, N_EXPERIMENTS)

plt.figure("Regret per Time Step")

plt.ylabel("Instantaneous Regret per Time Step")

plt.xlabel("Time")

plt.plot(regret_per_time_step_UCB_SW, 'yellow')

plt.plot(regret_per_time_step_UCB_CD, 'magenta')

plt.plot(regret_per_time_step_UCB, 'green')

plt.plot(regret_per_time_step_EXP3, 'orange')

plt.legend(["UCB-SW","UCB-CD", "UCB1"])

plt.savefig("graphs/step_6/regret_per_time_step.png")

plt.show()

cumulative_regret_per_time_step_UCB_SW = harry_plotter.calculate_accumulated_experiment_sums(SW_UCB_objective_regret_per_experiment, N_EXPERIMENTS)
cumulative_regret_per_time_step_UCB_CD = harry_plotter.calculate_accumulated_experiment_sums(CD_UCB_objective_regret_per_experiment, N_EXPERIMENTS)
cumulative_regret_per_time_step_UCB = harry_plotter.calculate_accumulated_experiment_sums(UCB_objective_regret_per_experiment, N_EXPERIMENTS)
cumulative_regret_per_time_step_EXP3 = harry_plotter.calculate_accumulated_experiment_sums(EXP3_objective_regret_per_experiment, N_EXPERIMENTS)

#Plot cumulative regret

plt.figure(6)

plt.ylabel("Cumulative Regret per Time Step")

plt.xlabel("Time")

plt.plot(cumulative_regret_per_time_step_UCB_CD, 'magenta')

plt.plot(cumulative_regret_per_time_step_UCB, 'green')

plt.plot(cumulative_regret_per_time_step_UCB_SW, 'yellow')

plt.plot(cumulative_regret_per_time_step_EXP3, 'orange')

plt.legend(["UCB-CD","UCB", "UCB-SW", "EXP3"])

plt.savefig("graphs/step_6/cumulative_regret_per_time_step.png")

plt.show()
#Plot instantaneous reward


