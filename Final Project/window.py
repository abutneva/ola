from abstract import mock_process, clairvoyant_process
from util import colours, plot_helper
import matplotlib.pyplot as plt
import numpy as np
#Utilities
colour = colours()
harry_plotter = plot_helper()
from collections import Counter

def calculate_mean_total_reward_per_experiment(rewards_per_experiment: list):
    mean_total_reward_per_experiment = []
    for experiment in rewards_per_experiment:
        mean_total_reward_per_experiment.append(np.mean(experiment))
    return mean_total_reward_per_experiment

def simulate_window(window_size: int):
    '''
    Function returns:
    - the mean reward per experiment [0]
    - the moving averages of the reward per time step [1]
    - the standard deviation of the reward per time step [2]
    - the total reward of the whole simulation [3]
    '''

    #Hyperparameters
    N_ARMS = 5 #We learn pricing
    TIME_HORIZON = 365
    N_EXPERIMENTS = 80
    SEASONS_CHANGE_EVERY = 121
    SEASONS = ["beachbody", "new year resolution", "lazy"]


    USED_ALGORITHM = "SW-UCB"
    Mock_UCB_SW = mock_process(N_ARMS, USED_ALGORITHM, "C1", window_size=window_size)
    #SW_UCB
    SW_UCB_rewards_per_experiment = []
    SW_UCB_cumulative_rewards_per_experiment = []
    SW_UCB_objective_rewards_per_experiment = []
    SW_UCB_objective_cumulative_rewards_per_experiment = []
    SW_UCB_objective_regret_per_experiment = []
    cum_rew = 0



    # Clairvoyant
    CV_rewards_per_experiment = []
    CV_cumulative_rewards_per_experiment = []
    Mock_CV = clairvoyant_process(N_ARMS, "C1")
    clairvoyant_arm_and_reward = Mock_CV.pull_best_arm()


    Mock_UCB_SW.initialize()
    counter = 0
    for t in range(0, TIME_HORIZON):

        if t % SEASONS_CHANGE_EVERY == 0:
            SEASON = SEASONS[counter % len(SEASONS)]
            counter += 1
            Mock_UCB_SW.change_configuration(SEASON)
            Mock_CV.change_configuration(SEASON)
            clairvoyant_arm_and_reward = Mock_CV.pull_best_arm()
            clairvoyant_arm = clairvoyant_arm_and_reward[0]
            clairvoyant_reward = clairvoyant_arm_and_reward[1]


        for e in range(0, N_EXPERIMENTS):

           
            predicted_value_SW_UCB = Mock_UCB_SW.pull_arm() # esCtimated by MAB
            predicted_value_as_price_SW_UCB = Mock_UCB_SW.return_price_for_arm(predicted_value_SW_UCB)
            target_value_SW_UCB = Mock_UCB_SW.get_target_value(predicted_value_as_price_SW_UCB) # real value
           

            Objective_UCB_SW_Reward = Mock_CV.find_best_bid(predicted_value_as_price_SW_UCB)[1]

            #Calculating Regret
            Objective_UCB_SW_Regret = clairvoyant_reward - Objective_UCB_SW_Reward

            # update parameters for UCB_SW
            parameters_UCB_SW = Mock_UCB_SW.update_parameters(target_value_SW_UCB, predicted_value_SW_UCB, cum_rew)

            CV_rewards_per_experiment.append(clairvoyant_reward)

            #SW-UCB
            SW_UCB_rewards_per_experiment.append(target_value_SW_UCB)
            SW_UCB_cumulative_rewards_per_experiment.append(sum(SW_UCB_rewards_per_experiment))
            SW_UCB_objective_rewards_per_experiment.append(Objective_UCB_SW_Reward)
            SW_UCB_objective_cumulative_rewards_per_experiment.append(sum(SW_UCB_objective_rewards_per_experiment))
            SW_UCB_objective_regret_per_experiment.append(Objective_UCB_SW_Regret)

    moved_mean_UCB_SW, moved_standard_UCB_SW = harry_plotter.calculate_moving_average(SW_UCB_objective_rewards_per_experiment, N_EXPERIMENTS)
    print(f"Simulation with window size {colour.cyanify(str(window_size))} is done")
    mean_per_experiment = calculate_mean_total_reward_per_experiment([SW_UCB_objective_rewards_per_experiment])
    return (mean_per_experiment[0], moved_mean_UCB_SW, moved_standard_UCB_SW, sum(SW_UCB_objective_rewards_per_experiment))

