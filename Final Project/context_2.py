from abstract import clairvoyant_process, mock_process_advertisment, mock_process
from util import colours, plot_helper, context_helper
import matplotlib.pyplot as plt
import numpy as np
import random
from collections import Counter
import warnings
from sklearn.exceptions import ConvergenceWarning
from abstract_context import SplitConditionContextGenerator, context_bandit_wrapper

'''
Idea of the context process:
    - Use the joint process
    - Every CHANGE_CLASSES_EVERY time steps we change the class of the advertisement bandits

'''

#Utilities
colour = colours()
plotter = plot_helper()
contextualizer = context_helper()


#Getting rid of convergence warnings but note we should asjust the GP parameters
warnings.filterwarnings("ignore", category=ConvergenceWarning)

#Hyperparameters for Advertisement
N_ARMS = 100 #We learn advertisement
LEARNING_COST = True
USED_ALGORITHM = "GP-UCB"


FEATURE_SETS = [(0,0), (0,1), (1,0), (1,1)]
DERIVED_CLASS_FROM_FEATURE_SET = contextualizer.get_class_for_feature_set(FEATURE_SETS[0])
CHANGE_CLASSES_EVERY = 2

#Scenatio 2: "Clases are not known" => We need to learn the context, still the classes are decisive for the reward and are just not known to the context generator 
#Initailize the context generator
GENERATE_CONTEXT_EVERY = 7
context_generator = SplitConditionContextGenerator()

Mock_Advertisement_UCB = mock_process_advertisment(N_ARMS, "C1", USED_ALGORITHM, LEARNING_COST)
USED_ALGORITHM = "GP-TS"
Mock_Advertisement_TS = mock_process_advertisment(N_ARMS, "C1", USED_ALGORITHM, LEARNING_COST)

#Context Bandit Wrapper
Bandits_UCB = context_bandit_wrapper("GP-UCB", {"C1": [(0,0), (0,1), (1,0), (1,1)]})
Bandits_TS = context_bandit_wrapper("GP-TS", {"C1": [(0,0), (0,1), (1,0), (1,1)]})
#Hyperparameters for the Learning Process
TIME_HORIZON = 365
N_EXPERIMENTS = 5
#Rewards
UCB_Bandit_rewards_per_experiment = []
UCB_Bandit_pulled_arms_per_experiment = []
TS_Bandit_rewards_per_experiment = []
TS_Bandit_pulled_arms_per_experiment = []

#Hyperparameters for Pricing
N_ARMS = 5 #We learn pricing
USED_ALGORITHM = "TS"
Mock_Pricing = mock_process(N_ARMS, USED_ALGORITHM, "C1")
PULL_PRICE_EVERY = 2

# Clairvoyant reward was defined as the sum of the clairrvoyant 
Mock_Clairvoyant = clairvoyant_process(5, "C2")
clairvoyant_candidate = Mock_Clairvoyant.get_optimal_candidate("joint")
clairvoyant_bid = clairvoyant_candidate[1]
clairvoyant_price = clairvoyant_candidate[0]
clairvoyant_reward = clairvoyant_candidate[2]
print(clairvoyant_reward)
clairvoyant_bid_as_arm = Mock_Advertisement_TS.return_arm_for_bid(clairvoyant_bid)
clairvoyant_price_as_arm = Mock_Pricing.return_arm_for_price(clairvoyant_price)

#Cumulative Reward 
Cumulative_Clairvoyant_Reward = []

#Initialize the context generator
#context_generator.initialize()

#Initializing of both bandits => Pulling each arm once and updating the GP
""" Mock_Advertisement_TS.initialize()
Mock_Advertisement_UCB.initialize() """

for t in range(0, TIME_HORIZON):
    #Updating the models
    print(colour.cyanify(f"Time step {t} / {TIME_HORIZON}"))
    if t % 2 == 0 and t != 0:
        #Prevent the the update method is called in when the bandits have no initial data
        Mock_Advertisement_TS.update_model()
        Mock_Advertisement_UCB.update_model()
        Bandits_UCB.update_model(FEATURE_SET)
        Bandits_TS.update_model(FEATURE_SET)
        
    
    # PULLING PRICE
    if t % PULL_PRICE_EVERY == 0:
        print("----------------------------------")
        predicted_price_as_arm = Mock_Pricing.pull_arm() #ARM of PRICE
        #print(colour.redify(f"TS-Pricing | pulled arm {predicted_price_as_arm}"))
        predicted_arm_as_price = Mock_Pricing.return_price_for_arm(predicted_price_as_arm) # PRICE
        #print(colour.redify(f"TS-Pricing | pulled price {predicted_arm_as_price}"))
    if t % GENERATE_CONTEXT_EVERY == 0 and t != 0:
        new_context = context_generator.generate_context()
        if new_context:
            print(colour.yellowfy("Context has been detected!"))
            #Update the context bandit
            Bandits_UCB.change_structure(new_context)
            Bandits_TS.change_structure(new_context)
    # GENERATING CONTEXT
        print("----------------------------------")
        # CHANGING CLASS
    if t % CHANGE_CLASSES_EVERY == 0:
        # Assign CLASS randomly out of CLASSES
        FEATURE_SET = random.choice(FEATURE_SETS)
        CLASS = contextualizer.get_class_for_feature_set(FEATURE_SET)
    for e in range(0, N_EXPERIMENTS):
        # THOMPSON SAMPLING
        #print("----------------------------------")
        predicted_value_Wrapper_UCB = Bandits_UCB.pull_arm(FEATURE_SET)
        #print(f"Pulled ARM of BANDIT {predicted_value_Wrapper_UCB}")
        predicted_value_Wrapper_TS = Bandits_TS.pull_arm(FEATURE_SET)
        #print(f"Pulled ARM of BANDIT {predicted_value_Wrapper_TS}")
        UCB_Bandit_pulled_arms_per_experiment.append(predicted_value_Wrapper_UCB)
        TS_Bandit_pulled_arms_per_experiment.append(predicted_value_Wrapper_TS)
        predicted_value_UCB = Mock_Advertisement_UCB.pull_arm() # estimated by MAB => Returns the bid not its index
        predicted_value_TS = Mock_Advertisement_TS.pull_arm() # estimated by MAB => Returns the bid not its index
        predicted_value_as_bid_Wrapper_UCB = Mock_Advertisement_TS.return_bid_for_arm(predicted_value_Wrapper_UCB) #Note TS instad of wrapper Bandit does this operation
        predicted_value_as_bid_Wrapper_TS = Mock_Advertisement_TS.return_bid_for_arm(predicted_value_Wrapper_TS) #Note TS instad of wrapper Bandit does this operation
        predicted_value_as_bid_UCB = Mock_Advertisement_UCB.return_bid_for_arm(predicted_value_UCB)
        predicted_value_as_bid_TS = Mock_Advertisement_TS.return_bid_for_arm(predicted_value_TS)
        #print(f"time {t} | experiment {e}")
        #print(colour.cyanify(f"GP-UCB | pulled bid {round(predicted_value_as_bid_UCB, 2)} | arm {predicted_value_UCB}"))
        #print(colour.greenify(f"GP-TS  | pulled bid {round(predicted_value_as_bid_TS, 2)} | arm {predicted_value_TS}"))
        #print(colour.yellowfy(f"Wrapper UCB| pulled bid {round(predicted_value_as_bid_Wrapper_UCB, 2)} | arm {predicted_value_Wrapper_UCB}"))
        #print(colour.yellowfy(f"Wrapper TS | pulled bid {round(predicted_value_as_bid_Wrapper_TS, 2)} | arm {predicted_value_Wrapper_TS}"))
        # then, get the real (randomized reward for comparison)
        target_value_UCB = Mock_Advertisement_UCB.get_reward(CLASS, predicted_arm_as_price, predicted_value_as_bid_UCB) # real value
        target_value_TS = Mock_Advertisement_TS.get_reward(CLASS, predicted_arm_as_price, predicted_value_as_bid_TS) # real value
        target_value_Wrapper_UCB = Mock_Advertisement_TS.get_reward(CLASS, predicted_arm_as_price, predicted_value_as_bid_Wrapper_UCB) # Have to resort to the TS Bandit for the reward calculation
        target_value_Wrapper_TS = Mock_Advertisement_TS.get_reward(CLASS, predicted_arm_as_price, predicted_value_as_bid_Wrapper_TS) # Have to resort to the TS Bandit for the reward calculation
        UCB_Bandit_rewards_per_experiment.append(target_value_Wrapper_UCB)
        TS_Bandit_rewards_per_experiment.append(target_value_Wrapper_TS)
        #Change this so that the conv prob of every user class is used 
        Mock_Pricing.update_parameters(Mock_Pricing.configuration["price_curve_for_user"][CLASS][predicted_arm_as_price], predicted_price_as_arm) # real value


        Mock_Advertisement_UCB.update_parameters(target_value_UCB, predicted_value_UCB) # real value

        Mock_Advertisement_TS.update_parameters( target_value_TS, predicted_value_TS)
        #Update the Wrapper Bandit

        Bandits_UCB.update_parameters(FEATURE_SET, target_value_Wrapper_UCB, predicted_value_Wrapper_UCB)# real value
        Bandits_TS.update_parameters(FEATURE_SET, target_value_Wrapper_TS, predicted_value_Wrapper_TS)# real value
        #Apply Context Generator
        F1, F2 = FEATURE_SET
        context_generator.update(((F1, F2), target_value_UCB))
        #CLAIRVOYANT Reward Cumulation
        Cumulative_Clairvoyant_Reward.append(clairvoyant_reward)
    


print(colour.greenify("UCB pulled arms:"))
print(Counter(Mock_Advertisement_UCB.pulled_arms))
print(colour.greenify("TS pulled arms:"))
print(Counter(Mock_Advertisement_TS.pulled_arms))
print(colour.redify("Accumulated rewards:"))
print(f"UCB: {round(sum(Mock_Advertisement_UCB.collected_rewards), 2)}")
print(f"TS: {round(sum(Mock_Advertisement_TS.collected_rewards), 2)}")
print(f"Wrapper UCB: {round(sum(UCB_Bandit_rewards_per_experiment), 2)}")
print(f"Wrapper TS: {round(sum(TS_Bandit_rewards_per_experiment), 2)}")

print(colour.yellowfy("Contexts Generator results:"))
print(f"Contexts Generator has predicted {context_generator.get_no_of_splits()} splits")
context_generator.get_detailed_split_recording()

average_reward_TS = plotter.calculate_average_reward(Mock_Advertisement_TS.collected_rewards, Mock_Advertisement_TS.pulled_arms)
average_reward_UCB = plotter.calculate_average_reward(Mock_Advertisement_UCB.collected_rewards, Mock_Advertisement_UCB.pulled_arms)
average_reward_Wrapper_UCB = plotter.calculate_average_reward(UCB_Bandit_rewards_per_experiment, UCB_Bandit_pulled_arms_per_experiment)
average_reward_Wrapper_TS = plotter.calculate_average_reward(TS_Bandit_rewards_per_experiment, TS_Bandit_pulled_arms_per_experiment)
plt.figure(0)

plt.ylabel("Cumulative Reward Advertisement")

plt.xlabel("Experiment")

#plt.plot(np.cumsum(np.mean(np.array([Mock_Advertisement_TS.collected_rewards]), axis = 0)), 'green')

#plt.plot(np.cumsum(np.mean(np.array([Mock_Advertisement_UCB.collected_rewards]), axis = 0)), 'blue')

plt.plot(np.cumsum(np.mean(np.array([Cumulative_Clairvoyant_Reward]), axis = 0)), 'b')

plt.plot(np.cumsum(np.mean(np.array([UCB_Bandit_rewards_per_experiment]), axis = 0)), 'green')

plt.plot(np.cumsum(np.mean(np.array([TS_Bandit_rewards_per_experiment]), axis = 0)), 'cyan')
#plt.plot(np.cumsum(np.mean(np.array([CV_rewards_per_experiment]), axis = 0)), 'b')

plt.legend(["GP-TS Wrapped", "GP-UCB Wrapped", "Clairvoyant"])

plt.savefig('graphs/step_4/unknown_structure/cumulative_reward_per_experiment.png')

plt.show()

moved_mean_TS, moved_standard_TS = plotter.calculate_moving_average(TS_Bandit_rewards_per_experiment, N_EXPERIMENTS)

x_axis = np.arange(1, len(moved_mean_TS) + 1)

plt.figure(1)

plt.plot(x_axis, moved_mean_TS, label = "GP-TS Average reward", color = "cyan")

plt.errorbar(x_axis, moved_mean_TS, yerr = moved_standard_TS, label = "TS Standard Deviation", linestyle = 'None', marker = 'o', color = "grey")

plt.xlabel("Time Steps")

plt.ylabel("Reward")

plt.title("Wrapped GP-TS Average Reward incl. Standard Deviation")

plt.legend()

plt.savefig('graphs/step_4/unknown_structure/moving_average_TS.png')

plt.show()

#Plotting each reward over time for UCB including its respective standard deviation
moved_mean_UCB, moved_standard_UCB = plotter.calculate_moving_average(UCB_Bandit_rewards_per_experiment, N_EXPERIMENTS)
# Generating values along the x axis
x_axis = np.arange(1, len(moved_mean_UCB) + 1)

plt.figure(2)

plt.plot(x_axis, moved_mean_UCB, label = "GP-UCB Average reward", color = "green")

plt.errorbar(x_axis, moved_mean_UCB, yerr = moved_standard_UCB, label = "GP-UCB Standard Deviation", linestyle = 'None', marker = 'o', color = "grey")

plt.xlabel("Time Steps")

plt.ylabel("Reward")

plt.title("GP-UCB Average Reward incl. Standard Deviation")

plt.legend()

plt.savefig('graphs/step_4/unknown_structure/moving_average_UCB.png')

plt.show()
#Plot instantaneous reward


reward_per_time_step_TS = plotter.calculate_experiment_sums(TS_Bandit_rewards_per_experiment, N_EXPERIMENTS)
reward_per_time_step_UCB = plotter.calculate_experiment_sums(UCB_Bandit_rewards_per_experiment, N_EXPERIMENTS)
reward_per_time_step_CV = plotter.calculate_experiment_sums(Cumulative_Clairvoyant_Reward, N_EXPERIMENTS)

#Plot instantaneous reward

plt.figure(3)

plt.ylabel("Instantaneous Reward per Time Step")

plt.xlabel("Time")

plt.plot(reward_per_time_step_TS, 'cyan')

plt.plot(reward_per_time_step_UCB, 'green')

plt.plot(reward_per_time_step_CV, 'blue')

""" plt.plot(np.mean(np.array([UCB_objective_rewards_per_experiment]), axis = 0), 'g')

plt.plot(np.mean(np.array([CV_rewards_per_experiment]), axis = 0), 'b') """

plt.legend(["Wrapped GP Thompson Sampling", "Wrapped GP UCB", "Clairvoyant"])

plt.savefig('graphs/step_4/unknown_structure/instantaneous_reward_per_time_step.png')

plt.show()

cumulative_reward_per_time_step_TS = plotter.calculate_accumulated_experiment_sums(TS_Bandit_rewards_per_experiment, N_EXPERIMENTS)
cumulative_reward_per_time_step_UCB = plotter.calculate_accumulated_experiment_sums(UCB_Bandit_rewards_per_experiment, N_EXPERIMENTS)
cumulative_reward_per_time_step_CV = plotter.calculate_accumulated_experiment_sums(Cumulative_Clairvoyant_Reward, N_EXPERIMENTS)

#Plot instantaneous reward

plt.figure(4)

plt.ylabel("Cumulative Reward per Time Step")

plt.xlabel("Time")

plt.plot(cumulative_reward_per_time_step_TS, 'cyan')

plt.plot(cumulative_reward_per_time_step_UCB, 'green')

plt.plot(cumulative_reward_per_time_step_CV, 'blue')

""" plt.plot(np.mean(np.array([UCB_objective_rewards_per_experiment]), axis = 0), 'g')

plt.plot(np.mean(np.array([CV_rewards_per_experiment]), axis = 0), 'b') """

plt.legend(["Wrapped GP-TS", "Wrapped GP-UCB", "Clairvoyant"])

plt.savefig('graphs/step_4/unknown_structure/cumulative_reward_per_time_step.png')

plt.show()

plt.figure(1)

plt.ylabel("Instantaneous Reward Advertisement")

plt.xlabel("Time")

#plt.plot(np.mean(np.array([Mock_Advertisement_TS.collected_rewards]), axis = 0), 'r')

#plt.plot(np.mean(np.array([Mock_Advertisement_UCB.collected_rewards]), axis = 0), 'g')

plt.plot(np.mean(np.array([TS_Bandit_rewards_per_experiment]), axis = 0), 'cyan')

plt.plot(np.mean(np.array([UCB_Bandit_rewards_per_experiment]), axis = 0), 'green')

plt.plot(np.mean(np.array([Cumulative_Clairvoyant_Reward]), axis = 0), 'blue')

plt.legend(["Wrapped GP-TS", "Wrapped GP-UCB", "Clairvoyant"])

plt.savefig('graphs/step_4/unknown_structure/instantaneous_reward_per_experiment.png')

plt.show()

#Plotting the cost/click curve based on rewards retrieved by the algorithms for every bid


plt.figure(3)

plt.ylabel("Reward per arm for usual GP-TS")

plt.xlabel("Arm")

plt.scatter(average_reward_TS.keys(), average_reward_TS.values())

plt.legend(["GP-TS"])

plt.savefig('graphs/step_4/unknown_structure/cost_per_arm_TS.png')

plt.show()

plt.figure(4)

plt.ylabel("Reward per arm for usual GP-UCB")

plt.xlabel("Arm")

plt.scatter(average_reward_UCB.keys(), average_reward_UCB.values())

plt.legend(["GP-UCB"])

plt.savefig('graphs/step_4/unknown_structure/cost_per_arm_UCB_usual.png')

plt.show()



plt.figure(4)

plt.ylabel("Cost per arm for Wrapper GP-UCB")

plt.xlabel("Arm")

plt.scatter(average_reward_Wrapper_UCB.keys(), average_reward_Wrapper_UCB.values())

plt.legend(["Mean Reward per arm for wrapper GP-UCB"])

plt.savefig('graphs/step_4/unknown_structure/cost_per_arm_UCB_wrapper.png')

plt.show()


plt.figure(4)

plt.ylabel("Cost per arm for Wrapper GP-TS")

plt.xlabel("Arm")

plt.scatter(average_reward_Wrapper_TS.keys(), average_reward_Wrapper_TS.values())

plt.legend(["Mean Reward per arm for wrapper GP-TS"])

plt.savefig('graphs/step_4/unknown_structure/cost_per_arm_TS_wrapper.png')

plt.show()




