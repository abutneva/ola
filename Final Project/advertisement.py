from abstract import clairvoyant_process, mock_process_advertisment
from util import colours, plot_helper
import matplotlib.pyplot as plt
import matplotlib.colors as mcolors
import numpy as np
#Utilities
colour = colours()
harry_plotter = plot_helper()

from collections import Counter
import warnings
#Getting rid of convergence warnings but note we should asjust the GP parameters
from sklearn.exceptions import ConvergenceWarning
warnings.filterwarnings("ignore", category=ConvergenceWarning)

#Hyperparameters
N_ARMS = 100 #We learn advertisement
LEARNING_COST = False
USED_ALGORITHM = "GP-UCB"
Mock_UCB = mock_process_advertisment(N_ARMS, "C1", USED_ALGORITHM, LEARNING_COST)
USED_ALGORITHM = "GP-TS"
Mock_TS = mock_process_advertisment(N_ARMS, "C1", USED_ALGORITHM, LEARNING_COST)
TIME_HORIZON = 365
N_EXPERIMENTS = 5
UCB_rewards_per_experiment = []
UCB_cumulative_rewards_per_experiment = []
UCB_objective_rewards_per_experiment = []
UCB_objective_cumulative_rewards_per_experiment = []
TS_rewards_per_experiment = []  
TS_objective_rewards_per_experiment = []
TS_cumulative_rewards_per_experiment = []
TS_objective_cumulative_rewards_per_experiment = []
TS_Regrets = []
UCB_Regrets = []

UPDATE_EVERY = 10

#Clairvoyant
Mock_Clairvoyant = clairvoyant_process(5, "C1")
clairvoyant_candidate = Mock_Clairvoyant.get_optimal_candidate("advertising")
clairvoyant_bid = clairvoyant_candidate[1]
clairvoyant_price = clairvoyant_candidate[0]
#Clairvoyant Reward is not the reward returned from its reward function but the amount of clicks or cost generated with the optimal candidate
if LEARNING_COST:
    clairvoyant_reward = Mock_Clairvoyant.configuration["cost_curve"][clairvoyant_bid]
else: 
    clairvoyant_reward = Mock_Clairvoyant.configuration["click_curve"][clairvoyant_bid]

#Cumulative Reward for the Initialisation
Clairvoyant_Rewards = [clairvoyant_reward for i in range(0, 100)]

#Initializing of both bandits => Pulling each arm once and updating the GP
Mock_UCB.initialize()
Mock_TS.initialize()

for t in range(0, TIME_HORIZON):
    print(colour.cyanify("----------------"))
    print(f"Time {t+1}/ {TIME_HORIZON}")
    Mock_UCB.update_model()
    Mock_TS.update_model()
    for e in range(0, N_EXPERIMENTS):
        # THOMPSON SAMPLING
        predicted_value_UCB = Mock_UCB.pull_arm() # estimated by MAB => arm
        predicted_value_TS = Mock_TS.pull_arm() # estimated by MAB  => arm
        predicted_value_as_bid_UCB = Mock_UCB.return_bid_for_arm(predicted_value_UCB)
        predicted_value_as_bid_TS = Mock_TS.return_bid_for_arm(predicted_value_TS)
        """ Mock_UCB.arm_counter[predicted_value_UCB] += 1
        Mock_TS.arm_counter[predicted_value_TS] += 1 """
        """ print(f"time {t} | experiment {e}")
        print(colour.cyanify(f"GP-UCB | pulled bid {round(predicted_value_as_bid_UCB, 2)} | arm {predicted_value_UCB}"))
        print(colour.greenify(f"GP-TS  | pulled bid {round(predicted_value_as_bid_TS, 2)} | arm {predicted_value_TS}")) """
        # then, get the real (randomized reward for comparison)
        target_value_UCB = Mock_UCB.get_target_value(predicted_value_UCB) # real value
        target_value_TS = Mock_TS.get_target_value(predicted_value_TS) # real value
        Mock_UCB.update_parameters(target_value_UCB, predicted_value_UCB) # real value
        Mock_TS.update_parameters( target_value_TS, predicted_value_TS) # real value

        #Regret
        UCB_Regret = clairvoyant_reward - target_value_UCB
        UCB_Regrets.append(UCB_Regret)

        TS_Regret = clairvoyant_reward - target_value_TS
        TS_Regrets.append(TS_Regret)
        if e % UPDATE_EVERY:
            pass


        Clairvoyant_Rewards.append(clairvoyant_reward)

    


if LEARNING_COST:
    LEARNING_TARGET = "cost"
else:
    LEARNING_TARGET = "clicks"

print(colour.greenify("UCB pulled arms:"))
print(Counter(Mock_UCB.pulled_arms))
print(colour.greenify("TS pulled arms:"))
print(Counter(Mock_TS.pulled_arms))
print(colour.redify("Accumulated rewards:"))
print(f"UCB: {round(sum(Mock_UCB.collected_rewards), 2)}")
print(f"TS: {round(sum(Mock_TS.collected_rewards), 2)}")

average_reward_TS = harry_plotter.calculate_average_reward(Mock_TS.collected_rewards, Mock_TS.pulled_arms)
average_reward_UCB = harry_plotter.calculate_average_reward(Mock_UCB.collected_rewards, Mock_UCB.pulled_arms)
plt.figure(0)

plt.ylabel("Cumulative Reward per Experiment")

plt.xlabel("Experiment")

plt.plot(np.cumsum(np.mean(np.array([Mock_TS.collected_rewards]), axis = 0)), 'cyan')

plt.plot(np.cumsum(np.mean(np.array([Mock_UCB.collected_rewards]), axis = 0)), 'green')

plt.plot(np.cumsum(np.mean(np.array([Clairvoyant_Rewards]),axis= 0)), 'blue')

plt.legend(["GP-TS", "GP-UCB", "Clairvoyant"])

plt.savefig(f'graphs/step_2/{LEARNING_TARGET}/cumulative_reward_per_experiment.png')

plt.show()




#Plot instantaneous reward

plt.figure(1)

plt.ylabel("Instantaneous Reward per Experiment")

plt.xlabel("Time")

plt.plot(np.mean(np.array([Mock_TS.collected_rewards]), axis = 0), 'cyan')

plt.plot(np.mean(np.array([Mock_UCB.collected_rewards]), axis = 0), 'green')

plt.plot(np.mean(np.array([Clairvoyant_Rewards]),axis= 0), 'blue')

plt.legend(["GP-TS", "GP-UCB", "Clairvoyant"])

plt.savefig(f'graphs/step_2/{LEARNING_TARGET}/instantaneous_reward_per_experiment.png')

plt.show()


#Plotting each reward over time for TS including its respective standard deviation

moved_mean_TS, moved_standard_TS = harry_plotter.calculate_moving_average(Mock_TS.collected_rewards, N_EXPERIMENTS)

x_axis = np.arange(1, len(moved_mean_TS) + 1)

plt.figure(1)

plt.plot(x_axis, moved_mean_TS, label = "GP-TS Moving Average reward", color='cyan')

plt.errorbar(x_axis, moved_mean_TS, yerr = moved_standard_TS, label = "TS Standard Deviation", linestyle = 'None', marker = 'o', color='grey')

plt.xlabel("Time Steps")

plt.ylabel("Reward")

plt.title("GP-TS Average Reward incl. Standard Deviation")

plt.legend()

plt.savefig(f'graphs/step_2/{LEARNING_TARGET}/TS_average_reward_incl_standard_deviation.png')

plt.show()

#Plotting each reward over time for UCB including its respective standard deviation
moved_mean_UCB, moved_standard_UCB = harry_plotter.calculate_moving_average(Mock_UCB.collected_rewards, N_EXPERIMENTS)
# Generating values along the x axis
x_axis = np.arange(1, len(moved_mean_UCB) + 1)

plt.figure(2)

plt.plot(x_axis, moved_mean_UCB, label = "GP-UCB Average reward")

plt.errorbar(x_axis, moved_mean_UCB, yerr = moved_standard_UCB, label = "GP-UCB Standard Deviation", linestyle = 'None', marker = 'o', color='grey')

plt.xlabel("Time Steps")

plt.ylabel("Reward")

plt.title("GP-UCB Average Reward incl. Standard Deviation")

plt.legend()

plt.savefig(f'graphs/step_2/{LEARNING_TARGET}/UCB_average_reward_incl_standard_deviation.png')

plt.show()


reward_per_time_step_TS = harry_plotter.calculate_experiment_sums(Mock_TS.collected_rewards, N_EXPERIMENTS)
reward_per_time_step_UCB = harry_plotter.calculate_experiment_sums(Mock_UCB.collected_rewards, N_EXPERIMENTS)
reward_per_time_step_CV = harry_plotter.calculate_experiment_sums(Clairvoyant_Rewards, N_EXPERIMENTS)

#Plot instantaneous reward

plt.figure(3)

plt.ylabel("Instantaneous Reward per Time Step")

plt.xlabel("Time")

plt.plot(reward_per_time_step_TS, 'cyan')

plt.plot(reward_per_time_step_UCB, 'green')

plt.plot(reward_per_time_step_CV, 'blue')

""" plt.plot(np.mean(np.array([UCB_objective_rewards_per_experiment]), axis = 0), 'g')

plt.plot(np.mean(np.array([CV_rewards_per_experiment]), axis = 0), 'b') """

plt.legend(["GP Thompson Sampling", "GP UCB", "Clairvoyant"])

plt.savefig(f'graphs/step_2/instantaneous_reward_per_time_step.png')

plt.show()

cumulative_reward_per_time_step_TS = harry_plotter.calculate_accumulated_experiment_sums(Mock_TS.collected_rewards, N_EXPERIMENTS)
cumulative_reward_per_time_step_UCB = harry_plotter.calculate_accumulated_experiment_sums(Mock_UCB.collected_rewards, N_EXPERIMENTS)
cumulative_reward_per_time_step_CV = harry_plotter.calculate_accumulated_experiment_sums(Clairvoyant_Rewards, N_EXPERIMENTS)

#Plot instantaneous reward

plt.figure(4)

plt.ylabel("Cumulative Reward per Time Step")

plt.xlabel("Time")

plt.plot(cumulative_reward_per_time_step_TS, 'cyan')

plt.plot(cumulative_reward_per_time_step_UCB, 'green')

plt.plot(cumulative_reward_per_time_step_CV, 'blue')

""" plt.plot(np.mean(np.array([UCB_objective_rewards_per_experiment]), axis = 0), 'g')

plt.plot(np.mean(np.array([CV_rewards_per_experiment]), axis = 0), 'b') """

plt.legend(["GP Thompson Sampling", "GP UCB", "Clairvoyant"])

plt.savefig(f'graphs/step_2/{LEARNING_TARGET}/cumulative_reward_per_time_step.png')

plt.show()


#Plotting the cost/click curve based on rewards retrieved by the algorithms for every bid




pull_counts_normalized_TS = harry_plotter.normalize_arm_counts(Mock_TS.arm_counter)
cmap = mcolors.LinearSegmentedColormap.from_list("mycmap", ["red", "green"])


plt.figure(2)

plt.ylabel(f"{LEARNING_TARGET} per arm")
plt.xlabel("Arm")

# Scatter plot
scatter = plt.scatter(average_reward_TS.keys(), average_reward_TS.values(), c=pull_counts_normalized_TS, cmap=cmap)

plt.colorbar(label='Number of times pulled')
plt.legend(["GP-TS"])

plt.savefig(f"graphs/step_2/{LEARNING_TARGET}/Arm_Dist_TS.png")

plt.show()

pull_counts_normalized_UCB = harry_plotter.normalize_arm_counts(Mock_UCB.arm_counter)


plt.figure(3)

plt.ylabel(f"{LEARNING_TARGET} per arm")

plt.xlabel("Arm")

plt.scatter(average_reward_UCB.keys(), average_reward_UCB.values(), c=pull_counts_normalized_UCB, cmap=cmap)

plt.colorbar(label='Number of times pulled')

plt.legend(["GP-UCB"])

plt.savefig(f"graphs/step_2/{LEARNING_TARGET}/Arm_Dist_UCB.png")

plt.show()

regret_per_time_step_TS = harry_plotter.calculate_experiment_sums(TS_Regrets, N_EXPERIMENTS)
regret_per_time_step_UCB = harry_plotter.calculate_experiment_sums(UCB_Regrets, N_EXPERIMENTS)

#Plot instantaneous regret

plt.figure(5)

plt.ylabel("Instantaneous Regret per Time Step")

plt.xlabel("Time")

plt.plot(regret_per_time_step_TS, 'cyan')

plt.plot(regret_per_time_step_UCB, 'green')

plt.legend(["Thompson Sampling", "UCB1"])

plt.savefig(f'graphs/step_2/{LEARNING_TARGET}/instantaneous_regret_per_time_step.png')

plt.show()

cumulative_regret_per_time_step_TS = harry_plotter.calculate_accumulated_experiment_sums(TS_Regrets, N_EXPERIMENTS)
cumulative_regret_per_time_step_UCB = harry_plotter.calculate_accumulated_experiment_sums(UCB_Regrets, N_EXPERIMENTS)

#Plot cumulative regret

plt.figure(6)

plt.ylabel("Cumulative Regret per Time Step")

plt.xlabel("Time")

plt.plot(cumulative_regret_per_time_step_TS, 'cyan')

plt.plot(cumulative_regret_per_time_step_UCB, 'green')

plt.legend(["Thompson Sampling", "UCB1"])

plt.savefig(f'graphs/step_2/{LEARNING_TARGET}/cumulative_regret_per_time_step.png')

plt.show()
